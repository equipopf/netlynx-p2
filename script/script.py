import sys
import os
import time

import RPi.GPIO as GPIO
import serial

GPIO.setmode(GPIO.BCM)
GPIO.setup(26, GPIO.OUT) 

port = serial.Serial("/dev/ttyS0", baudrate=115200, timeout=1)
 
port.write('AT'+'\r\n') 
time.sleep(1)
        
respuesta = port.read(10)

if "OK" in respuesta:
	print "El GPRS esta encendido!"
		
else:
	GPIO.output(26, False) 
	time.sleep(1)
	GPIO.output(26, True) 
	time.sleep(2)
	GPIO.output(26, False) 
	time.sleep(3)

GPIO.cleanup() # this ensures a clean exit 

os.system("sudo umount -d /dev/sda1")
time.sleep(8)
os.system("sudo mount -t vfat /dev/sda1 /media/pi/RAYG")
os.system("sudo rm -r /media/pi/RAYG/mongod.lock")
time.sleep(8)
os.system("sudo mongod --dbpath /media/pi/RAYG &")
time.sleep(8)
os.system("node /home/pi/Desktop/proyecto/web/desarrollo/visual &")
time.sleep(8)
os.system("cd /home/pi/Desktop/proyecto/sniffer; sudo ./sniffer D Mymodel &")

