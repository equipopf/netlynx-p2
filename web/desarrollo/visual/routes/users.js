const express = require('express');

//Se importa el router de express
//Se usa para definir las rutas que se usaran
const router = express.Router();

const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const User = require('../models/user');


//Register
router.post('/register', (req, res, next) =>{
	
	let newUser = new User({
		name: req.body.name,
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		answer:req.body.answer,
		preguntas:req.body.Preguntas
		});
	
	User.addUser(newUser, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to register user'});
				}
			else{
				res.json({success: true, msg: 'User registered'});
				}
		});	
	
	});
	/**/
	router.get('/connectionclients', (req, res, next) =>{
	User.getConnectionsClients((err, connection)=>{
		if(err) throw err;
		if(!connection){
			return res.json({success: false, msg: 'No connections found'});
			}
		res.json({
					success: true,
					connection:connection

					});
		});
	});
	
	/**/
	
	
	router.post('/updateclients', (req, res, next) =>{
	let newClient = new User({
		name: req.body.nameup,
		email: req.body.emailup,
		username:req.body.usernameup
		});

	User.getUpdateClient(newClient,req.body.id, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to update username'});
				}
			else{
				res.json({success: true, msg: 'User update'});
				}
		});	
	});
	/**/
	
	router.post('/clientdelete', (req, res, next) =>{
	
User.getDeleteClient(req.body.id, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to delete username'});
				}
			else{
				res.json({success: true, msg: 'User delete'});
				}
		});	
	});
	
	/**/
router.post('/authenticateClient', (req, res, next) =>{

	const password = req.body.oldpassword;

	User.getUserByIdClient(req.body.id, (err, user) => {
		if(err) throw err;
		if(!user){
			return res.json({success: false, msg: 'User not found'});
			}
		User.comparePassword(password, user.password, (err, isMatch) =>{
			
			if(err) throw err;
			if(isMatch){
		  	 res.json({
					success: true, msg: 'success password'
					});
				}
				else{
					return res.json({success: false, msg: 'Wrong password'});
					}
			
			});
		});
	});	 
	
	
	
	/**/
	//aqui estoy trabajado
		router.post('/updateUsername', (req, res, next) =>{
	let newClientPassword = new User({
		username: req.body.usernameup,
		answer: req.body.answer,
		password:req.body.NewPasswordREstablecida,
		preguntas:req.body.Preguntas
		});
	   User.getUserByUsername(newClientPassword.username, (err, user) => {
		if(err) throw err;
		if(!user){
			return res.json({success: false, msg: 'User not found'});
			}
		console.log("estoy aqui");
		
		    console.log(newClientPassword.preguntas);
			console.log(newClientPassword.answer);
			console.log(user.preguntas);
			console.log(user.answer);
			console.log(user.password);
		User.comparePreguntas(newClientPassword.preguntas, user.preguntas, (err, isMatch) =>{
			
			if(err) throw err;
			if(isMatch){
		
		
		User.compareanswer(newClientPassword.answer, user.answer, (err, isMatch) =>{
			
			if(err) throw err;
			if(isMatch){
				
			User.getUpdatePassword(newClientPassword,user._id, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to update password'});
				}
			else{
				res.json({success: true, msg: 'User update'});
				}
		});	
			
			
				}
				else{
					return res.json({success: false, msg: 'Wrong answer'});
					}
			
			});
		
				}
				else{
					return res.json({success: false, msg: 'Wrong preguntas'});
					}
			
			}); 
			
		///////////////////////////////////////////////////////////////
		});
	});
	
	
	/**/
	router.post('/updateclientswithpassword', (req, res, next) =>{
	let newClient = new User({
		name: req.body.nameup,
		email: req.body.emailup,
		username:req.body.usernameup,
		password:req.body.newpassword
		});

	User.getUpdateClientwithPassword(newClient,req.body.id, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to update username'});
				}
			else{
				res.json({success: true, msg: 'User update'});
				}
		});	
	});
	
	/**/
//Authenticate
router.post('/authenticate', (req, res, next) =>{
	const username = req.body.username;
	const password = req.body.password;
	
	User.getUserByUsername(username, (err, user) => {
		if(err) throw err;
		if(!user){
			return res.json({success: false, msg: 'User not found'});
			}
			
		User.comparePassword(password, user.password, (err, isMatch) =>{
			
			if(err) throw err;
			if(isMatch){
				const token = jwt.sign({data:user}, config.secret, { /*se cambio de user a {data:user}*/
				expiresIn:86400 //1 week
				});
				
				res.json({
					success: true,
					token: 'JWT '+token,
					user: {
						id: user._id,
						name: user.name,
						username: user.username,
						email: user.email
						}
					});
				}
				else{
					return res.json({success: false, msg: 'Wrong password'});
					}
			
			});
		});
	});
/*	
//Authenticate
router.get('/authenticate', (req, res, next) =>{
	res.send('AUTHENTICATE');
	});
*/
//Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) =>{
	res.json({user: req.user});
	});
	
/*
//Validate
router.get('/validate', (req, res, next) =>{
	res.send('VALIDATE');
	});
*/
//Se exporta el router
module.exports=router;
