const express = require('express');

//Se importa el router de express
//Se usa para definir las rutas que se usaran
const router = express.Router();

const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const State = require('../models/state');


router.get('/state', (req, res, next) =>{
	State.getStates((err, state)=>{
		if(err) throw err;
		if(!state){
			return res.json({success: false, msg: 'No states found'});
			}
			
	
		res.json({
					success: true,
					state:state
					});
		
		});

	});
	

	
router.post('/resetState', (req,res, next) =>{
	console.log("jose 1");
	let newState = new State({
		cant_anomalies: req.body.cant_anomalies,
		net_state: req.body.net_state
		
		});
	
	State.resetStates(newState, (err, state) => {
			if(err){
				 return {success: false, msg: 'Failed to update state'};
				}
			else{
				 return {success: true, msg: 'State updated'};
				}
		});	
	});
	
module.exports=router;	

