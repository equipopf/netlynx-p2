const express = require('express');

//Se importa el router de express
//Se usa para definir las rutas que se usaran
const router = express.Router();

const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const Connection = require('../models/connection');

	
router.get('/connection', (req, res, next) =>{
	Connection.getConnections((err, connection)=>{
		if(err) throw err;
		if(!connection){
			return res.json({success: false, msg: 'No connections found'});
			}
			
			
		res.json({
					success: true,
					connection:connection
					/*connection: {
						
						addressa: connection.addressa,
						porta: connection.porta,
						addressb: connection.addressb,
						portb: connection.portb,
						packets: connection.packets,
						duration: connection.duration,
						urgent: connection.urgent,
						state: connection.state
						}*/
					});
		
		});
	
	
	
	
	});
/*
router.post('/connection', (req, res, next) =>{
	let newConnection = new Connection({
	addressa: req.body.addressa,
	porta: req.body.porta,
	addressb: req.body.addressb,
	portb: req.body.portb,
	packets: req.body.packets,
	duration: req.body.duration,
	urgent: req.body.urgent,
	state: req.body.state
		
		});
		
	Connection.obtainConnection(newConnection, (err, connection)=> {
		if(err){
			res.json({success: false, msg: 'Failed to register connection'});
			} else{
			res.json({success: true, msg: 'Connection registered'});
				
				
				}
		});
	});
	*/
//Se exporta el router
module.exports=router;	
