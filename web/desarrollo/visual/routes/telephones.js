const express = require('express');

const router = express.Router();

const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');
const Telephone = require('../models/telephone');


router.post('/telephoneregistation', (req, res, next) =>{
	let newTelephone = new Telephone({
		owner: req.body.owner,
		telephone: req.body.telephone,
		company:req.body.company
		});
	/*no esta terminado*/
	Telephone.addTelephone(newTelephone, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to register telephone'});
				}
			else{
				res.json({success: true, msg: 'Telephone registered'});
				}
		});	
	
	});

/////////////*/////////////id:this.id,

router.post('/telephonerupdate', (req, res, next) =>{
	let newTelephone = new Telephone({
		owner: req.body.ownerup,
		telephone: req.body.telephoneup,
		company:req.body.companyup
		});
		/*console.log(req.body.id);
		console.log(newTelephone.id);*/
Telephone.getUpdateTelephone(newTelephone,req.body.id, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to update telephone'});
				}
			else{
				res.json({success: true, msg: 'Telephone update'});
				}
		});	
	});
	
	
	
router.post('/telephonerdelete', (req, res, next) =>{
	
Telephone.getDeleteTelephone(req.body.id, (err, user) => {
			if(err){
				res.json({success: false, msg: 'Failed to delete telephone'});
				}
			else{
				res.json({success: true, msg: 'telephone delete'});
				}
		});	
	});
/***************/
	
router.get('/connectiontelephones', (req, res, next) =>{
	Telephone.getConnectionsTelephone((err, connection)=>{
		if(err) throw err;
		if(!connection){
			return res.json({success: false, msg: 'No connections found'});
			}
		res.json({
					success: true,
					connection:connection

					});
		
		});
	
	
	
	
	});
	
	

	
module.exports=router;
