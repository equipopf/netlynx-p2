const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt =require('passport-jwt').ExtractJwt;
const User =require('../models/user');
const config =require('../config/database');
//revisar si lo de "User.getUserById(jwt_payload._doc._id, (err, user) =>{" esta bien
module.exports =function(passport){
	let opts={};
	opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt'); //esta es el sustituto de fromAuthHeader()
	opts.secretOrKey = config.secret;
	passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
		//console.log(jwt_payload);
		User.getUserById(jwt_payload.data._id, (err, user) =>{ /*Podria ser jwt_payload._id*/ /* Se cambio a jwt_payload.data._id,*/
			if(err){
				return done(err, false);
			}
			
			if(user){
				return done(null, user);
			}
			else{
				return done(null, false);
				}
			});
		}));	
}
