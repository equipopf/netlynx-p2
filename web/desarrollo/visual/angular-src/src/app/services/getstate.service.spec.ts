import { TestBed, inject } from '@angular/core/testing';

import { GetstateService } from './getstate.service';

describe('GetstateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetstateService]
    });
  });

  it('should be created', inject([GetstateService], (service: GetstateService) => {
    expect(service).toBeTruthy();
  }));
});
