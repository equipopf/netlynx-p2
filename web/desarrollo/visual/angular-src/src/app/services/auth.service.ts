import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {map} from 'rxjs/operators';
import {tokenNotExpired} from 'angular2-jwt';



//Ya fue inportado lo necesario para token
@Injectable({
  providedIn: 'root'
})
export class AuthService {
	authToken: any;
	user: any;
	domain: string = 'http://192.168.0.10:3000';


  constructor(private http:Http) { }
  
  
  
  registerUser(user){
	  let headers = new Headers();
	  headers.append('Content-Type','application/json');
	  return this.http.post(this.domain + '/users/register', user, {headers: headers}).pipe(map(res => res.json()));
  }
  
  authenticateUser(user){
		let headers = new Headers();
	  headers.append('Content-Type','application/json');
	  return this.http.post(this.domain + '/users/authenticate', user, {headers: headers}).pipe(map(res => res.json()));
	
  }
  
  
  
    authenticateUserClient(user){
		let headers = new Headers();
	  headers.append('Content-Type','application/json');
	  return this.http.post(this.domain + '/users/authenticateClient', user, {headers: headers}).pipe(map(res => res.json()));
	
  }
  
  getProfile(){
	let headers = new Headers();
	this.loadToken();
	headers.append('Authorization', this.authToken);
	headers.append('Content-Type','application/json');
	
	return this.http.get(this.domain + '/users/profile', {headers: headers}).pipe(map(res => res.json()));
  }
  
  /**/
  getConnectionClient(){
  let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.get(this.domain + '/users/connectionclients', {headers: headers}).pipe(map(res => res.json()));
  }
  
  updateClient(client){
    let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.post(this.domain + '/users/updateclients', client, {headers: headers}).pipe(map(res => res.json()));
  }
  
  updateClientWithPassword(client){
    let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.post(this.domain + '/users/updateclientswithpassword', client, {headers: headers}).pipe(map(res => res.json()));
  }
  
   deleteCLient(client){
   let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.post(this.domain + '/users/clientdelete', client, {headers: headers}).pipe(map(res => res.json()));
  }
  /**/
  
   updateUsername(userPassword){
    let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.post(this.domain + '/users/updateUsername', userPassword, {headers: headers}).pipe(map(res => res.json()));
  }
  
  
  /**/
  storeUserData(token, user){
  localStorage.setItem('id_token', token);
  localStorage.setItem('user', JSON.stringify(user));
  this.authToken=token;
  this.user=user;
  }
  
  loadToken(){
  const token = localStorage.getItem('id_token');
  this.authToken=token;
  }
  
  loggedIn(){
	return tokenNotExpired('id_token');
  }
  
  logout(){
  this.authToken=null;
  this.user=null;
  localStorage.clear();
  }
  
 
  
}
