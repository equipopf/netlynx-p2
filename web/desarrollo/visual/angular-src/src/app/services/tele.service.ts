import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeleService {
	
 telephone:any;
 domain: string = 'http://192.168.0.10:3000';
 
  constructor(private http:Http) { }
  
  registerTelephone(telephoner){
	  let headers = new Headers();
	  headers.append('Content-Type','application/json');
	  return this.http.post(this.domain + '/telephones/telephoneregistation', telephoner, {headers: headers}).pipe(map(res => res.json()));
  }
  
  
 getConnectionTelephone(){
	let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.get(this.domain + '/telephones/connectiontelephones', {headers: headers}).pipe(map(res => res.json()));
  } 
 
 
   
  updateTelephone(telephoner){
  
	  let headers = new Headers();
	  headers.append('Content-Type','application/json');
	  return this.http.post(this.domain + '/telephones/telephonerupdate', telephoner, {headers: headers}).pipe(map(res => res.json()));
  }
  
  
  deleteTelephone(telephoner){
   let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.post(this.domain + '/telephones/telephonerdelete', telephoner, {headers: headers}).pipe(map(res => res.json()));
  }
}
