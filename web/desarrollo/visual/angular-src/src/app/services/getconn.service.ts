import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetconnService {
	connection:any;
	domain: string = 'http://192.168.0.10:3000';

  constructor(private http:Http) { }
  
  
getConnection(){
	let headers = new Headers();
	headers.append('Content-Type','application/json');
	return this.http.get(this.domain + '/connections/connection', {headers: headers}).pipe(map(res => res.json()));
  }
}


