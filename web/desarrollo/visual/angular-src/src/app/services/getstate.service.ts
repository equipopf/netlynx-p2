import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetstateService {
	state: any;
	domain: string = 'http://192.168.0.10:3000';

  constructor(private http:Http) { }
  
getState(){
		let headers = new Headers();
		headers.append('Content-Type','application/json');
		return this.http.get(this.domain + '/states/state', {headers: headers}).pipe(map(res => res.json()));
	}
	
reset(state){
	console.log("borrando service");
		let headers = new Headers();
		headers.append('Content-Type','application/json');
		return this.http.post(this.domain + '/states/resetState', state,{headers: headers}).pipe(map(res => res.json()));
		
	}
	
}
