import { TestBed, inject } from '@angular/core/testing';

import { GetconnService } from './getconn.service';

describe('GetconnService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetconnService]
    });
  });

  it('should be created', inject([GetconnService], (service: GetconnService) => {
    expect(service).toBeTruthy();
  }));
});
