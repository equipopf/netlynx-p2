import { TestBed, inject } from '@angular/core/testing';

import { TeleService } from './tele.service';

describe('TeleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TeleService]
    });
  });

  it('should be created', inject([TeleService], (service: TeleService) => {
    expect(service).toBeTruthy();
  }));
});
