
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { WebsocketService } from './websocket.service';
import {map} from 'rxjs/operators';


const CHAT_URL = 'ws://192.168.0.10:8000/';
export interface Message {
	
	por_serror_rate:string,
	por_srv_rate:string,
	por_diffsrv_rate:string,
	por_srv_diff_host_rate:string,
	cant_dst_host:string,
	por_cant_dstsrc_srv:string,
	por_srv_serror:string,
	por_rerror_rate:string,
	por_srv_rerror:string
	
		
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {
	public messages: Subject<Message>;

	constructor(wsService: WebsocketService) {
		this.messages = <Subject<Message>>wsService
			.connect(CHAT_URL)
			.pipe(map((response: MessageEvent): Message => {
				let data = JSON.parse(response.data);
				return {
					por_serror_rate:data.por_serror_rate,
					por_srv_rate:data.por_srv_rate,
					por_diffsrv_rate:data.por_diffsrv_rate,
					por_srv_diff_host_rate:data.por_srv_diff_host_rate,
					cant_dst_host:data.cant_dst_host,
					por_cant_dstsrc_srv:data.por_cant_dstsrc_srv,
					por_srv_serror:data.por_srv_serror,
					por_rerror_rate:data.por_rerror_rate,
					por_srv_rerror:data.por_srv_rerror
	
				}
			}));
	}
}
