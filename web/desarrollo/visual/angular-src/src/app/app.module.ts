import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DetailsComponent } from './components/details/details.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ConnectionsComponent } from './components/connections/connections.component';
import { TelephoneregistrationComponent } from './components/telephoneregistration/telephoneregistration.component';
import { RestorepasswordComponent } from './components/restorepassword/restorepassword.component';


import {ValidateService} from './services/validate.service';
import {FlashMessagesModule} from 'angular2-flash-messages';
import {AuthService} from './services/auth.service';
import {GetconnService} from './services/getconn.service';
import {TeleService} from './services/tele.service';
import {HttpModule} from '@angular/http';
import {GetstateService} from './services/getstate.service';

import{AuthGuard} from './guards/auth.guard';
import { WebsocketService } from './services/websocket.service';
import { ChatService } from './services/chat.service';

import {DataTableModule} from "angular-6-datatable";
import {NgxMaskModule} from 'ngx-mask';


const appRoutes: Routes= [
	{path:'details', component: DetailsComponent, canActivate:[AuthGuard]},
	{path:'register', component: RegisterComponent, canActivate:[AuthGuard]},
	{path:'', component: LoginComponent},
	{path:'dashboard', component: DashboardComponent, canActivate:[AuthGuard]},
	{path:'profile', component: ProfileComponent, canActivate:[AuthGuard]},
	{path:'connections', component: ConnectionsComponent, canActivate:[AuthGuard]},
	{path:'telephoneregistration', component: TelephoneregistrationComponent, canActivate:[AuthGuard]},
	{path:'restorepassword', component: RestorepasswordComponent}	
]

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    LoginComponent,
    RegisterComponent,
    DetailsComponent,
    DashboardComponent,
    ProfileComponent,
    ConnectionsComponent,
    TelephoneregistrationComponent,
    RestorepasswordComponent
  ],
  imports: [
    BrowserModule,
    NgSelectModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule.forRoot(),
    DataTableModule,
    NgxMaskModule.forRoot()
    
  ],
  providers: [ValidateService, AuthService, AuthGuard, WebsocketService, ChatService, GetconnService, TeleService, GetstateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
