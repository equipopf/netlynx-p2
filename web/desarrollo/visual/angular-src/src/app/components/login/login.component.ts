import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	username: String;
	password: String;
	
	
  constructor(private authService:AuthService, private router: Router, private flashMessage:FlashMessagesService,private validateService: ValidateService) { }

  ngOnInit() {
  
  if(this.authService.loggedIn()){
			this.router.navigate(['/dashboard']);	
			
		}
  
  }
  
  onLoginSubmit(){
	const user={
		username: this.username,
		password: this.password
	}
	
	if(!this.validateService.validateUser(user)){
		this.flashMessage.show('Por favor, llene todos los campos', {cssClass: 'alert-danger', timeout:5000});
		return false;
		}
	
	this.authService.authenticateUser(user).subscribe(data=>{
		if(data.success){
			this.authService.storeUserData(data.token, data.user);
			this.flashMessage.show('Has iniciado sesión', {cssClass: 'alert-success', timeout:5000});
			this.router.navigate(['/dashboard']);
			console.log("a dashboard");
			
		}
		else{
			this.flashMessage.show('Las credenciales no son correctas', {cssClass: 'alert-danger', timeout:5000});
			this.router.navigate(['/']);
			console.log("no entra");
		}
	});
  }

}
