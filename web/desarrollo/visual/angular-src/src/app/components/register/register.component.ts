import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service'
import {FlashMessagesService} from 'angular2-flash-messages';
import { AuthService } from '../../services/auth.service'
import {Router} from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
	name: String;
	username: String;
	email: String;
	password: String;
	answer:String;
	Preguntas:String;
	againpassword:String;
	opciones = [
		'¿Cuál es el nombre de la escuela primaria a la que asististe?',
		'¿A qué hora del día naciste? (hh:mm)',
		'¿En qué ciudad se conocieron tus padres?',
		'¿Cuál era el nombre de tu profesor/a de 3ero de primaria?',
		'¿Cuál es el nombre de la escuela secundaria a que asistió tu madre?',
		'¿En cuál hospital nació tu hijo/a más grande?',
		'¿Cuál era tu apodo en tu infancia?'
	 ]


	constructor(private validateService: ValidateService, private flashMessage:FlashMessagesService, private authService: AuthService, private router:Router) { }

	ngOnInit() {
	   this.clear();
	}
	
    clear(){
    this.name ="";
	this.username ="";
	this.password ="";
	this.email="";
	this.answer="";
	this.againpassword="";
	this.Preguntas=null;
    }
    
	onRegisterSubmit(){
		const user={
			name: this.name,
			email: this.email,
			username: this.username,
			password: this.password,
			answer:this.answer,
			Preguntas:this.Preguntas,
			againpassword:this.againpassword
		}

     // console.log(this.Preguntas);

		if(!this.validateService.validateRegister(user)){
		this.flashMessage.show('Por favor, llene todos los campos', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}

		if(!this.validateService.validateEmail(user.email)){
		this.flashMessage.show('Por favor, use un correo electrónico válido', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}
		console.log(this.againpassword);
		console.log(user.password);
        if(!(this.againpassword==user.password)){
        this.flashMessage.show('La contraseña no coincide', {cssClass: 'alert-danger', timeout:3000});
		return false;
        }
		this.authService.registerUser(user).subscribe(data => {
			if(data.success){
			this.name ="";
			this.username ="";
			this.password ="";
			this.email="";
			this.answer="";
			this.againpassword="";
			this.Preguntas=null;
				this.flashMessage.show('Ya estás registrado y puedes iniciar sesión', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['/register']);
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['/register']);
				
			}
		});
		
		
	}

}


