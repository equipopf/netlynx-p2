import { Component, OnInit} from '@angular/core';
import {ChatService } from '../../services/chat.service';
import {WebsocketService } from '../../services/websocket.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
   providers: [ WebsocketService, ChatService ]
})
export class DetailsComponent implements OnInit {
	

	chart1= Chart;
	chart2= Chart;
	chart3= Chart;
	chart4= Chart;
	chart5= Chart;
	chart6= Chart;
	chart7= Chart;
	chart8= Chart;
	chart9= Chart;
	
	chart1b= Chart;
	chart2b= Chart;
	chart3b= Chart;
	chart4b= Chart;
	chart5b= Chart;
	chart6b= Chart;
	chart7b= Chart;
	chart8b= Chart;
	chart9b= Chart;
	
	data0: any;
	
	indicator=0;

  constructor(private chatService: ChatService) { 
	 console.log("intentando");
	chatService.messages.subscribe(msg => {	
	
	
			if(this.indicator==0){				  
				this.createchart();	
				this.indicator=1;	
			}
	
	this.data0= [
	parseFloat(msg.por_serror_rate),
	parseFloat(msg.por_srv_rate),
	parseFloat(msg.por_diffsrv_rate),
	parseFloat(msg.por_srv_diff_host_rate),
	parseFloat(msg.cant_dst_host),
	parseFloat(msg.por_cant_dstsrc_srv),
	parseFloat(msg.por_srv_serror),
	parseFloat(msg.por_rerror_rate),
	parseFloat(msg.por_srv_rerror)
];
	
	 this.actualizar()
	});
  }


  
  actualizar(){
	
	let thetime=new Date();
	let ss=thetime.getSeconds();
	let mm=thetime.getMinutes();
	let hh=thetime.getHours();
	
	let momento= hh + ':' + mm + ':' + ss + ':';
  
  this.chart1.data.labels.push(momento);
  this.chart1.data.datasets[0].data.push(this.data0[0]);
  this.chart1.update();
  
  this.chart1.data.labels.splice(0,1);
  this.chart1.data.datasets[0].data.splice(0,1);
  this.chart1.update();
  
  this.chart1b.data.labels.push(momento);
  this.chart1b.data.datasets[0].data.push(this.data0[0]);
  this.chart1b.update();
  
  this.chart1b.data.labels.splice(0,1);
  this.chart1b.data.datasets[0].data.splice(0,1);
  this.chart1b.update();
  
  
  this.chart2.data.labels.push(momento);
  this.chart2.data.datasets[0].data.push(this.data0[1]);
  this.chart2.update();
  
  this.chart2.data.labels.splice(0,1);
  this.chart2.data.datasets[0].data.splice(0,1);
  this.chart2.update();
  
  this.chart2b.data.labels.push(momento);
  this.chart2b.data.datasets[0].data.push(this.data0[1]);
  this.chart2b.update();
  
  this.chart2b.data.labels.splice(0,1);
  this.chart2b.data.datasets[0].data.splice(0,1);
  this.chart2b.update();
  
  
  this.chart3.data.labels.push(momento);
  this.chart3.data.datasets[0].data.push(this.data0[2]);
  this.chart3.update();
  
  this.chart3.data.labels.splice(0,1);
  this.chart3.data.datasets[0].data.splice(0,1);
  this.chart3.update();
  
  this.chart3b.data.labels.push(momento);
  this.chart3b.data.datasets[0].data.push(this.data0[2]);
  this.chart3b.update();
  
  this.chart3b.data.labels.splice(0,1);
  this.chart3b.data.datasets[0].data.splice(0,1);
  this.chart3b.update();
  
  this.chart4.data.labels.push(momento);
  this.chart4.data.datasets[0].data.push(this.data0[3]);
  this.chart4.update();
  
  this.chart4.data.labels.splice(0,1);
  this.chart4.data.datasets[0].data.splice(0,1);
  this.chart4.update();
  
  this.chart4b.data.labels.push(momento);
  this.chart4b.data.datasets[0].data.push(this.data0[3]);
  this.chart4b.update();
  
  this.chart4b.data.labels.splice(0,1);
  this.chart4b.data.datasets[0].data.splice(0,1);
  this.chart4b.update();
  
  
  this.chart5.data.labels.push(momento);
  this.chart5.data.datasets[0].data.push(this.data0[4]);
  this.chart5.update();
  
  this.chart5.data.labels.splice(0,1);
  this.chart5.data.datasets[0].data.splice(0,1);
  this.chart5.update();
  
  this.chart5b.data.labels.push(momento);
  this.chart5b.data.datasets[0].data.push(this.data0[4]);
  this.chart5b.update();
  
  this.chart5b.data.labels.splice(0,1);
  this.chart5b.data.datasets[0].data.splice(0,1);
  this.chart5b.update();
  
  this.chart6.data.labels.push(momento);
  this.chart6.data.datasets[0].data.push(this.data0[5]);
  this.chart6.update();
  
  this.chart6.data.labels.splice(0,1);
  this.chart6.data.datasets[0].data.splice(0,1);
  this.chart6.update();
  
  this.chart6b.data.labels.push(momento);
  this.chart6b.data.datasets[0].data.push(this.data0[5]);
  this.chart6b.update();
  
  this.chart6b.data.labels.splice(0,1);
  this.chart6b.data.datasets[0].data.splice(0,1);
  this.chart6b.update();
  
  
  this.chart7.data.labels.push(momento);
  this.chart7.data.datasets[0].data.push(this.data0[6]);
  this.chart7.update();
  
  this.chart7.data.labels.splice(0,1);
  this.chart7.data.datasets[0].data.splice(0,1);
  this.chart7.update();
  
  this.chart7b.data.labels.push(momento);
  this.chart7b.data.datasets[0].data.push(this.data0[6]);
  this.chart7b.update();
  
  this.chart7b.data.labels.splice(0,1);
  this.chart7b.data.datasets[0].data.splice(0,1);
  this.chart7b.update();
  
  
  this.chart8.data.labels.push(momento);
  this.chart8.data.datasets[0].data.push(this.data0[7]);
  this.chart8.update();
  
  this.chart8.data.labels.splice(0,1);
  this.chart8.data.datasets[0].data.splice(0,1);
  this.chart8.update();
  
  this.chart8b.data.labels.push(momento);
  this.chart8b.data.datasets[0].data.push(this.data0[7]);
  this.chart8b.update();
  
  this.chart8b.data.labels.splice(0,1);
  this.chart8b.data.datasets[0].data.splice(0,1);
  this.chart8b.update();
  
  
  this.chart9.data.labels.push(momento);
  this.chart9.data.datasets[0].data.push(this.data0[8]);
  this.chart9.update();
  
  this.chart9.data.labels.splice(0,1);
  this.chart9.data.datasets[0].data.splice(0,1);
  this.chart9.update();
  
  this.chart9b.data.labels.push(momento);
  this.chart9b.data.datasets[0].data.push(this.data0[8]);
  this.chart9b.update();
  
  this.chart9b.data.labels.splice(0,1);
  this.chart9b.data.datasets[0].data.splice(0,1);
  this.chart9b.update();
  
  
  
  
  }
  
  private message = {
	por_serror_rate:'0',
	por_srv_rate:'0',
	por_diffsrv_rate:'0',
	por_srv_diff_host_rate:'0',
	cant_dst_host:'0',
	por_cant_dstsrc_srv:'0',
	por_srv_serror:'0',
	por_rerror_rate:'0',
	por_srv_rerror:'0'
	}

  sendMsg() {
		console.log('new message from client to websocket: ', this.message);
		this.chatService.messages.next(this.message);
		this.message.por_serror_rate = '';	
	}
	  ngOnInit() {
	  
	 
	  
  }
  
  
  
  createchart(){
  
    
            Chart.defaults.global.defaultFontFamily='Lato';
            Chart.defaults.global.defaultFontSize=9;
            Chart.defaults.global.defaultFontColot='#777';
  
  this.chart1 = new Chart('par1', {
  
  type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_serror_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(255, 99, 132, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_serror_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			this.chart2 = new Chart('par2', {
  
				type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(54, 162, 235, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_srv_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			this.chart3 = new Chart('par3', {
  
  type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_diffsrv_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(255, 206, 86, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_diffsrv_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			this.chart4 = new Chart('par4', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_diff_host_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(75, 192, 192, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_srv_diff_host_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                         left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart5 = new Chart('par5', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'cant_dst_host',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(153, 102, 255, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'cant_dst_host',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                         left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart6 = new Chart('par6', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_cant_dstsrc_srv',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(255, 159, 64, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_cant_dstsrc_srv',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart7 = new Chart('par7', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_serror',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(150, 40, 27, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_srv_serror',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart8 = new Chart('par8', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_rerror_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(135, 211, 124, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_rerror_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart9 = new Chart('par9', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_rerror',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(145, 61, 136, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                   title:{
                       display:true,
                       text:'por_srv_rerror',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			 this.chart1b = new Chart('par1b', {
  
  type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_serror_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(255, 99, 132, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_serror_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			this.chart2b = new Chart('par2b', {
  
				type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(54, 162, 235, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_srv_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			this.chart3b = new Chart('par3b', {
  
  type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_diffsrv_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(255, 206, 86, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_diffsrv_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                         left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
			this.chart4b = new Chart('par4b', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_diff_host_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(75, 192, 192, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_srv_diff_host_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart5b = new Chart('par5b', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'cant_dst_host',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(153, 102, 255, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'cant_dst_host',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart6b = new Chart('par6b', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_cant_dstsrc_srv',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(255, 159, 64, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_cant_dstsrc_srv',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart7b = new Chart('par7b', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_serror',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(150, 40, 27, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_srv_serror',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart8b = new Chart('par8b', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_rerror_rate',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(135, 211, 124, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_rerror_rate',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
			
						this.chart9b = new Chart('par9b', {
  
				 type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'por_srv_rerror',
                            data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                            backgroundColor: 'rgba(145, 61, 136, 0.6)',   
                            borderWidth:1,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000'  
                    }]
                },
                options:{
                maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'por_srv_rerror',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						ticks: {
							suggestedMin: 0,
							suggestedMax: 100
						}                  
					  }]
                  }
                }
  
			});
  
  }
  
 
  
	
}
