import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import { ValidateService } from '../../services/validate.service'
import {FlashMessagesService} from 'angular2-flash-messages';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
user:Object;
public connection:any;
nameup:String;
usernameup:String;
emailup:String;
oldpassword:String;
newpassword:String;
public data:any;
againnewpassword:String;
 id:String;

  constructor(private authService:AuthService, private router:Router,private validateService: ValidateService,private flashMessage:FlashMessagesService) { }

cargarTabla(){
	this.authService.getConnectionClient().subscribe(connection=>{
	if(connection.success){
		this.connection=connection.connection;
		this.data=this.connection;
		}
	},
		err => {
		console.log(err);
		return false;
		});
  }

  ngOnInit() {
  this.cargarTabla();
	this.authService.getProfile().subscribe(profile=>{
	
		this.user=profile.user;
		
	},
	err=>{
		console.log(err);
		return false;
	
	});
  
  }
  
  /*update*/
  
  
 onRegisterUpdateClient(){
 		console.log("vamos bien 1");

const client={
    id:this.id,
	nameup:this.nameup,
	usernameup:this.usernameup,
	emailup:this.emailup,
	oldpassword:this.oldpassword,
	newpassword:this.newpassword,
	againnewpassword:this.againnewpassword
  }
  		/*console.log(client.nameup);
  		console.log(client.usernameup);
  		console.log(client.emailup);*/

		if(!this.validateService.validateRegisterUp(client)){
		this.flashMessage.show('Por favor, llene todos los campos', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}
		
		if(!this.validateService.validateEmailUp(client.emailup)){
		this.flashMessage.show('Por favor, use un correo electrónico válido', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}
		
		
		
		  if(!(this.againnewpassword==client.newpassword)){
        this.flashMessage.show('La contraseña no coincide', {cssClass: 'alert-danger', timeout:3000});
		return false;
        }
		
		if(!this.validateService.validateRegisterUpPasswordCampo(client)){
		
		if(!this.validateService.validateRegisterUpPassword(client)){
		this.flashMessage.show('Por favor, llene todos los campos', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}else{
		
		//cambio normal
		this.authService.updateClient(client).subscribe(data => {
			if(data.success){
				this.flashMessage.show('La contraseña fue modificada satisfactoriamente', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['/profile']);
				 this.cargarTabla();
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['/profile']);
				
			}
		});	
		}
		
		}else{
		//primero chequear la constrasena vieja
		this.authService.authenticateUserClient(client).subscribe(data=>{
		if(data.success){
		//cambio de constrasena y todos los demas datos	
			console.log("aqui estamos");
			
			this.authService.updateClientWithPassword(client).subscribe(data => {
			if(data.success){
				this.flashMessage.show('El usuario fue modificado satisfactoriamente', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['/profile']);
				 this.cargarTabla();
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['/profile']);
				
			}
		});
			
			
			
		}
		else{
			this.flashMessage.show('Usuario no encontrado', {cssClass: 'alert-danger', timeout:5000});
			this.router.navigate(['/profile']);
		}
	   });
		
		}
		

	
	 }
	 
	 
  search(term: string) {
  var term=term.replace(/ /g,'').toLowerCase();
  console.log(term);
  var key= term.split('=');
    if(!term) {
      this.data = this.connection;
     
    } else {
       
      switch(key[0]){
      
			case "nombre":
				this.data = this.connection.filter(x => x.name.trim().toLowerCase().includes(key[1]));
				break;
				
			case "usuario":	
				this.data = this.connection.filter(x => x.username.trim().toLowerCase().includes(key[1]));
				break;
				
			case "correoelectronico":
				this.data = this.connection.filter(x => x.email.trim().toLowerCase().includes(key[1]));
				break;
			default:
				break;
        }
      
     
    }
  }	 
	 
	 
	 

doUpdate(conn){
this.id=conn._id;
this.nameup=conn.name;
this.usernameup=conn.username;
this.emailup=conn.email;
this.oldpassword="";
this.newpassword="";
this.againnewpassword="";
 }
	
	
onRegisterDeleteClient(){
const Client={
   id:this.id
  }
		this.authService.deleteCLient(Client).subscribe(data => {
			if(data.success){
				this.flashMessage.show('El usuario fue eliminado satisfactoriamente', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['//profile']);
				 this.cargarTabla();
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['//profile']);
				
			}
		});	
}
	 
doDelete(conn){
this.id=conn._id;
} 
	
	
	
	 
}
