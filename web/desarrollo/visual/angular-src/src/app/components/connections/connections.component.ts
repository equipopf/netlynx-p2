import { Component, OnInit } from '@angular/core';
import {GetconnService} from '../../services/getconn.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-connections',
  templateUrl: './connections.component.html',
  styleUrls: ['./connections.component.css']
})
export class ConnectionsComponent implements OnInit {

public connection:any;
public data:any;

  constructor(private getconnService:GetconnService, private router:Router) {}

  ngOnInit() {
  

  /*setInterval(this.segundo(),500);*/
  this.segundo();
	/*this.getconnService.getConnection().subscribe(connection =>{
		this.connection=connection.connection;
		this.data=this.connection;
	},
	err => {
	console.log(err);
	return false;
	});*/

	
  }
  
  sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
	
  
  async segundo(){
  /*while(1){*/
  	this.getconnService.getConnection().subscribe(connection =>{
  	if(connection.success){
		this.connection=connection.connection;
		this.data=this.connection;
		console.log("yA ESTOY ARRIBE NITIDO");
		}
	},
	err => {
	console.log(err);
	return false;
	});
	
	/*await this.sleep(5000);*/
	
  /*}*/
  }
  search(term: string) {
  var term=term.replace(/ /g,'').toLowerCase();
  console.log(term);
  var key= term.split('=');
    if(!term) {
      this.data = this.connection;
     
    } else {
       
      switch(key[0]){
      
			case "direcciona":
				this.data = this.connection.filter(x => x.s_IP.trim().toLowerCase().includes(key[1]));
				break;
				
			case "puertoa":	
				this.data = this.connection.filter(x => x.s_port.trim().toLowerCase().includes(key[1]));
				break;
				
			case "direccionb":
				this.data = this.connection.filter(x => x.d_IP.trim().toLowerCase().includes(key[1]));
				break;
			
			case "puertob":	
				this.data = this.connection.filter(x => x.d_port.trim().toLowerCase().includes(key[1]));
				break;
				
			case "paquetes":	
				this.data = this.connection.filter(x => x.cant_mensajes.trim().toLowerCase().includes(key[1]));
				break;
				
			case "urgente":
				this.data = this.connection.filter(x => x.urgent.trim().toLowerCase().includes(key[1]));
				break;
				
			case "estado":	
				this.data = this.connection.filter(x => x.estado.trim().toLowerCase().includes(key[1]));
				break;
			
			default:
				break;
        }
      
     
    }
  }

}
	
