import { Component, OnInit } from '@angular/core';
import {WebsocketService } from '../../services/websocket.service';
import { Chart } from 'chart.js';
import {GetstateService} from '../../services/getstate.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service'
import * as ChartAnnotation from 'chartjs-plugin-annotation';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	chart1= Chart;
	
	state:any;
	
	color_icon:string;
	
	indicator=0;
	
 constructor(private validateService: ValidateService, private getstateService: GetstateService, private router:Router, private flashMessage:FlashMessagesService) {
	 

 /*
		chatService.messages.subscribe(msg => {			
     console.log("Response from websocket: " + msg);
      console.log('Response from websocket: ', msg);
      
      	
			if(this.indicator==0){				  
				this.createchart();	
				this.indicator=1;	
			}
           
           this.data0= [
                                parseFloat(msg.por_serror_rate),
                                parseFloat(msg.por_srv_rate),
								parseFloat(msg.por_diffsrv_rate),
                                parseFloat(msg.por_srv_diff_host_rate),
                                parseFloat(msg.cant_dst_host),
								parseFloat(msg.por_cant_dstsrc_srv),
								parseFloat(msg.por_srv_serror),
								parseFloat(msg.por_rerror_rate),
								parseFloat(msg.por_srv_rerror)
                            ];
                           
                           
            this.chart1.data.datasets[0].data=this.data0.slice(0);
			this.chart1.update();  
          
		});
		*/
		
		
	}

  
  actualizar(){
	
	let thetime=new Date();
	let ss=thetime.getSeconds();
	let mm=thetime.getMinutes();
	let hh=thetime.getHours();
	
	let momento= hh + ':' + mm + ':' + ss + ':';
  
  this.chart1.data.labels.push(momento);
  this.chart1.data.datasets[0].data.push(this.state.state_dec);
  this.chart1.update();
  
  this.chart1.data.labels.splice(0,1);
  this.chart1.data.datasets[0].data.splice(0,1);
  this.chart1.update();
  
  }
  
	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}
	
	async getAnomalies(){
	while(1){
	
	this.getstateService.getState().subscribe(state =>{
		
	  if(state.success){
	  
		this.state=state.state;
		
		if(this.state.net_state){
				this.color_icon= "rgba(242, 38, 19,0.8)";
		
			}
		else{
				this.color_icon= "rgba(0,230,64,0.8)";
		
			}

	if(this.indicator==0){				  
				this.createchart();	
				this.indicator=1;	
			}
	
	this.actualizar();
	}	
	},
	err => {
	console.log(err);
	return false;
	});
	
	
	
	await this.sleep(5000);
	
	
	}
	}
	
	eraseAnomalies(){
	
	const state_reset={
			cant_anomalies:0,
			net_state:0
		}
	
	console.log("por casualidad");
	this.getstateService.reset(state_reset).subscribe(data => {
	
			if(data.success){
				this.flashMessage.show('Los indicadores de anomalías han sido reiniciados', {cssClass: 'alert-success', timeout:3000});
			
			}
			else{
			
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
			}
		});
	}

  ngOnInit() {
		let namedChartAnnotation = ChartAnnotation;
		namedChartAnnotation["id"]="annotation";
		Chart.pluginService.register( namedChartAnnotation);
		this.getAnomalies();
	
  }
 
  
  
  
  createchart(){
  
    
            Chart.defaults.global.defaultFontFamily='Lato';
            Chart.defaults.global.defaultFontSize=9;
            Chart.defaults.global.defaultFontColot='#777';
  
  this.chart1 = new Chart('par1', {
  
  type:'line',
                data:{
                    labels:['00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00',
							'00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00'],
                    datasets:[{
                            label:'Probabilidad de existencia de aonomalías',
                            data:[0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
                                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
                                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
                                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,
                                  0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5,],
                            backgroundColor: 'rgba(54, 162, 235, 0.6)',   
                            borderWidth:2,
                            borderColor:'#777',
                            hoverBorderWidth:3,
                            hoverBorderColor:'#000', 
                            fill: false  
                    }]
                },
                options:{
					maintainAspectRatio:false,
					responsive: true,
                   title:{
                       display:true,
                       text:'Probabilidad de existencia de anomalías',
                       fontSize:14
                   }, 
                  legend:{
                     display:false,
                     position:'right',
                     labels:{
                         fontColot:'#000'
                     }
                  },
                  layout:{
                      padding:{
                          left:0,
                          right:0,
                          bottom:0,
                          top:0
                      }
                  },
                  scales: {
					  yAxes: [{
						
						ticks: {
							suggestedMin: 0,
							suggestedMax: 1
						}                  
					  }]
                  },
                  
                  annotation: {
					  annotations: [{
						type: 'line',
						mode: 'horizontal',
						scaleID: 'y-axis-0',
						value: 0.5,
						borderColor: 'rgba(242, 38, 19,0.8)',
						borderWidth: 4,
						label: {
						  enabled: false,
						  content: 'Umbral de anomalías'
						},
			
					  }]
				
					}
                }
  
			});
       
  
  }
  

}

