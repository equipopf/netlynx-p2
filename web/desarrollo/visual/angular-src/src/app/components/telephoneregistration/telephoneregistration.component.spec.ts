import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelephoneregistrationComponent } from './telephoneregistration.component';

describe('TelephoneregistrationComponent', () => {
  let component: TelephoneregistrationComponent;
  let fixture: ComponentFixture<TelephoneregistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelephoneregistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelephoneregistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
