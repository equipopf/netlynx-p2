import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service'
import {FlashMessagesService} from 'angular2-flash-messages';
import { TeleService } from '../../services/tele.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-telephoneregistration',
  templateUrl: './telephoneregistration.component.html',
  styleUrls: ['./telephoneregistration.component.css']
})
export class TelephoneregistrationComponent implements OnInit {
	
  owner:String;
  telephone:String;
  company:String;
  public connection:any;
  ownerup:String;
  telephoneup:String;
  companyup:String;
  id:String;
  public data:any;
  
 cargarTabla(){
	this.teleService.getConnectionTelephone().subscribe(connection=>{
	if(connection.success){
		this.connection=connection.connection;
		this.data=this.connection;
		}
	},
		err => {
		console.log(err);
		return false;
		});
  }
  
  constructor(private validateService: ValidateService, private flashMessage:FlashMessagesService, private teleService: TeleService, private router:Router) { }


  ngOnInit() {
		  this.cargarTabla();
  }
  
  
  
  onRegisterSubmit(){
  const telephoner={
   owner:this.owner,
   telephone:this.telephone,
   company:this.company
  }
  
 /* console.log(telephoner.owner);
    console.log(telephoner.telephone);
      console.log(telephoner.company);*/
      
  if(!this.validateService.validateRegisterTelephone(telephoner)){
		this.flashMessage.show('Please fill in all fields', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}
		
	this.teleService.registerTelephone(telephoner).subscribe(data => {
			if(data.success){
				 this.owner ="";
				this.telephone ="";
				this.company ="";
				this.cargarTabla();
				this.flashMessage.show('El número telefónico ha sido registrado', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['/telephoneregistration']);
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['/telephoneregistration']);
				
			}
		});	
		
  }
  
   search(term: string) {
  var term=term.replace(/ /g,'').toLowerCase();
  console.log(term);
  var key= term.split('=');
    if(!term) {
      this.data = this.connection;
     
    } else {
       
      switch(key[0]){
      
			case "owner":
				this.data = this.connection.filter(x => x.owner.trim().toLowerCase().includes(key[1]));
				break;
				
			case "telephone":	
				this.data = this.connection.filter(x => x.telephone.trim().toLowerCase().includes(key[1]));
				break;
				
			case "company":
				this.data = this.connection.filter(x => x.company.trim().toLowerCase().includes(key[1]));
				break;
			default:
				break;
        }
      
     
    }
  }	 
  
onRegisterUpdate(){
const telephoner={
   id:this.id,
   ownerup:this.ownerup,
   telephoneup:this.telephoneup,
   companyup:this.companyup

  }

  if(!this.validateService. validateUpdateTelephone(telephoner)){
		this.flashMessage.show('Por favor, llene todos los campos', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}
	
		this.teleService.updateTelephone(telephoner).subscribe(data => {
			if(data.success){
				this.flashMessage.show('El número telefónico ha sido actualizado', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['/telephoneregistration']);
				 this.cargarTabla();
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['/telephoneregistration']);
				
			}
		});	
	
	 }
	 
 doUpdate(conn){
this.id=conn._id;
this.ownerup=conn.owner;
this.telephoneup=conn.telephone;
this.companyup=conn.company;
 }
	 
onRegisterDelete(){
const telephoner={
   id:this.id
  }
		this.teleService.deleteTelephone(telephoner).subscribe(data => {
			if(data.success){
				this.flashMessage.show('El número telefónico ha sido eliminado', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['/telephoneregistration']);
				 this.cargarTabla();
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				this.router.navigate(['/telephoneregistration']);
				
			}
		});	
}
	 
doDelete(conn){
this.id=conn._id;
} 


}
