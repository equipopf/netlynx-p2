import { Component, OnInit } from '@angular/core';

import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {FlashMessagesService} from 'angular2-flash-messages';
import { ValidateService } from '../../services/validate.service'
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-restorepassword',
  templateUrl: './restorepassword.component.html',
  styleUrls: ['./restorepassword.component.css']
})
export class RestorepasswordComponent implements OnInit {

usernameup:String;
	answer:String;
	NewPasswordREstablecida:String;
	ReplyNewPasswordREstablecida:String;
	Preguntas:String;
	opciones = [
	'¿Cuál es el nombre de la escuela primaria a la que asististe?',
	'¿A qué hora del día naciste? (hh:mm)',
	'¿En qué ciudad se conocieron tus padres?',
	'¿Cuál era el nombre de tu profesor/a de 3ero de primaria?',
	'¿Cuál es el nombre de la escuela secundaria a que asistió tu madre?',
	'¿En cuál hospital nació tu hijo/a más grande?',
	'¿Cuál era tu apodo en tu infancia?'
	 ]

  constructor(private authService:AuthService, private router: Router, private flashMessage:FlashMessagesService,private validateService: ValidateService) { }

  ngOnInit() {
	
  }
  
  /*onSelectChange(val){
	this.Preguntas = val;
  }*/

onRegisterRestore(){
  const userPassword={
	usernameup:this.usernameup,
	answer:this.answer,
	NewPasswordREstablecida:this.NewPasswordREstablecida,
	ReplyNewPasswordREstablecida:this.ReplyNewPasswordREstablecida,
	Preguntas:this.Preguntas
  }
		/*console.log(this.answer);
		console.log(this.Preguntas);
		
		this.Preguntas = null;*/
		
		if(!this.validateService.validateCampoPassword(userPassword)){
		this.flashMessage.show('Por favor, llene todos los campos', {cssClass: 'alert-danger', timeout:3000});
		return false;
		}
		
		/*if(!(this.againnewpassword==client.newpassword)){
        this.flashMessage.show('La contraseña no coincide', {cssClass: 'alert-danger', timeout:3000});
		return false;
        }*/
		
		
		if(userPassword.NewPasswordREstablecida==userPassword.ReplyNewPasswordREstablecida){
			this.authService.updateUsername(userPassword).subscribe(data => {
			if(data.success){
				this.Preguntas = null;
				this.flashMessage.show('La contraseña ha sido reestablecida', {cssClass: 'alert-success', timeout:3000});
				this.router.navigate(['']);
				 
			}
			else{
				this.flashMessage.show('Ha ocurrido un error', {cssClass: 'alert-danger', timeout:3000});
				
				
			}
		});
  }else{
  this.flashMessage.show('La contraseña no coincide', {cssClass: 'alert-danger', timeout:3000});
		return false;
  }
  
  }
  

}
