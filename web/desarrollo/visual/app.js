const express = require('express');
const path =require('path');
const bodyParser = require('body-parser');
const cors= require('cors');
const passport =require('passport');
const mongoose =require('mongoose');
const config = require('./config/database');

//Se conecta a la base de datos
mongoose.connect(config.database);

//Saber si esta conectado con base de datos
mongoose.connection.on('connected', () => {
	console.log('Connected to database '+config.database);
	});
	
//Saber si hay un error al conectar con base de datos
mongoose.connection.on('error', (err) => {
	console.log('Database error '+err);
	});

//Se inicializa app con express
const app=express();

//Set Static Folder, acceso al directorio de Angular
app.use(express.static(path.join(__dirname, 'public')));


//Se importa el archivo users.js
const users =require('./routes/users');

//Se importa el archivo states.js
const states =require('./routes/states');

//Se importa el archivo connections.js
const connections =require('./routes/connections');

//Se importa el archivo telephones.js
const telephones =require('./routes/telephones');

//Port number 
const port=3000;

//Cors Middleware
app.use(cors());



// configurar cabeceras http
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

	next();
});


//Body Parser Middleware, devuelve el valor cuando se llama
app.use(bodyParser.json());

//Passport Middlware
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

//Se establece que se usara '/users' para las rutas de usuarios
app.use('/users', users);

//Se establece que se usara '/connections' para las rutas de usuarios
app.use('/connections', connections);

//Se establece que se usara '/states' para las rutas de estados
app.use('/states', states);

//Se establece que se usara '/connections' para las rutas de telephones
app.use('/telephones', telephones);

//Index Route
app.get('/', (req, res) =>{
	res.send('Invalid Endpoint');
	});
	
app.get('*',(req, res) => {
	res.sendFile(path.join(__dirname, 'public/index.html'));
	});


//Start Server
app.listen(port,() => {
	console.log('Server started on port '+port);
	
	});

