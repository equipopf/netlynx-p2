const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');

//User schema

const UserSchema = mongoose.Schema({
	name: {
		type:String
		},
		
	email: {
		type: String,
		required: true
		},
	username: {
		type: String,
		required: true
		},
		
	password: {
		type:String,
		required: true
		},
	answer: {
		type:String,
		required: true
		},
	preguntas: {
		type:String,
		required: true
		}
	});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function(id, callback){
	User.findById(id, callback);
	
	}
	
module.exports.getUserByUsername = function(username, callback){
	const query= {username: username}
	User.findOne(query, callback);
	
	}
	
	/**/
module.exports.getUserByIdClient= function(id, callback){
	console.log(id);
	//const query= {'id':id}
	//User.findOne(id, callback);
	User.findById(id,callback);
	//User.findByIdAndRemove(id,callback);	

	}	
	/**/
	
	/**/
module.exports.addUser = function(newUser, callback){
	console.log(newUser.password);
	console.log(newUser.preguntas);
	console.log(newUser.answer);
	bcrypt.genSalt(10, (err, salt)=>{
		
		bcrypt.hash(newUser.password, salt, (err, hash)=>{
			if(err) throw err;
			newUser.password=hash;	
			bcrypt.hash(newUser.preguntas, salt, (err, hash)=>{
			if(err) throw err;
			newUser.preguntas=hash;
			bcrypt.hash(newUser.answer, salt, (err, hash)=>{
			if(err) throw err;
			newUser.answer=hash;
			newUser.save(callback);
			});	
			});	
			});	
			
			
			
			
			
		
			
		});
	}

module.exports.comparePassword = function(candidatePassword, hash, callback){
		bcrypt.compare(candidatePassword, hash,(err, isMatch) =>{
			if(err) throw err;
			callback(null, isMatch);
			});
	
	}
	/**/
	
module.exports.comparePreguntas = function(candidatePreguntas, hash, callback){
		bcrypt.compare(candidatePreguntas, hash,(err, isMatch) =>{
			if(err) throw err;
			callback(null, isMatch);
			});
	
	}
	
	module.exports.compareanswer = function(candidateAnswer, hash, callback){
		bcrypt.compare(candidateAnswer, hash,(err, isMatch) =>{
			if(err) throw err;
			callback(null, isMatch);
			});
	
	}
	
	/**/
module.exports.getConnectionsClients = function(callback){
	User.find({},callback);
	}
		
	
	
module.exports.getUpdateClient = function(newClient,id,callback){
  console.log(id);

	User.findById( id, function( err, client){
			client.name = newClient.name;
			client.username = newClient.username;
			client.email = newClient.email;
			
			client.save(callback);
	});
	
	}	
	
	module.exports.getDeleteClient = function(id,callback){

	User.findByIdAndRemove(id,callback);
	
	}
	
	
	
module.exports.getUpdateClientwithPassword = function(newClient,id,callback){
  console.log(id);

bcrypt.genSalt(10, (err, salt)=>{
		bcrypt.hash(newClient.password, salt, (err, hash)=>{
			if(err) throw err;
			newClient.password=hash;
			
			User.findById( id, function( err, client){
			client.name = newClient.name;
			client.username = newClient.username;
			client.email = newClient.email;
			client.password=newClient.password;
			client.save(callback);
	});
			});	
		});	
	
	}	
/**/

	
module.exports.getUpdatePassword = function(newClient,id,callback){
  console.log(id);

bcrypt.genSalt(10, (err, salt)=>{
		bcrypt.hash(newClient.password, salt, (err, hash)=>{
			if(err) throw err;
			newClient.password=hash;
			User.findById( id, function( err, client){
			client.password=newClient.password;
			client.save(callback);
	});
			});	
		});	
	
	}	









