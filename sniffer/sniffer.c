#define __FAVOR_BSD       /* Using BSD TCP header          */ 
#define __USE_BSD /* Using BSD IP header           */
#include <pcap.h>
#include <stdio.h>
#include <net/ethernet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <limits.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <libwebsockets.h>
#include <bson.h>
#include <mongoc.h>
#include <stdio.h>

#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <float.h>
#include <stdarg.h>
#include <locale.h>
#include <stdbool.h>

#include <wiringPi.h>
#include <wiringSerial.h>

/*###################################################################################################################*/
/*GPRS*/

int call_t (char* tel);
void *get_telephone();


/*###################################################################################################################*/

/*###################################################################################################################*/
/*SVM*/
int min_int(int x, int y) { return (x<y)?x:y; }
double min_double(double x, double y) { return (x<y)?x:y; }

int max_int(int x, int y) { return (x>y)?x:y; }
double max_double(double x, double y) { return (x>y)?x:y; }


struct svm_node
{
	int index;
	double value;
};

enum { C_SVC, NU_SVC, ONE_CLASS, EPSILON_SVR, NU_SVR };	/* svm_type */
enum { LINEAR, POLY, RBF, SIGMOID, PRECOMPUTED }; /* kernel_type */

struct svm_parameter
{
	int svm_type;
	int kernel_type;
	int degree;	/* for poly */
	double gamma;	/* for poly/rbf/sigmoid */
	double coef0;	/* for poly/sigmoid */

	/* these are for training only */
	double cache_size; /* in MB */
	double eps;	/* stopping criteria */
	double C;	/* for C_SVC, EPSILON_SVR and NU_SVR */
	int nr_weight;		/* for C_SVC */
	int *weight_label;	/* for C_SVC */
	double* weight;		/* for C_SVC */
	double nu;	/* for NU_SVC, ONE_CLASS, and NU_SVR */
	double p;	/* for EPSILON_SVR */
	int shrinking;	/* use the shrinking heuristics */
	int probability; /* do probability estimates */
};

struct svm_model
{
	struct svm_parameter param;	/* parameter */
	int nr_class;		/* number of classes, = 2 in regression/one class svm */
	int l;			/* total #SV */
	struct svm_node **SV;		/* SVs (SV[l]) */
	double **sv_coef;	/* coefficients for SVs in decision functions (sv_coef[k-1][l]) */
	double *rho;		/* constants in decision functions (rho[k*(k-1)/2]) */
	double *probA;		/* pariwise probability information */
	double *probB;
	int *sv_indices;        /* sv_indices[0,...,nSV-1] are values in [1,...,num_traning_data] to indicate SVs in the training set */

	/* for classification only */

	int *label;		/* label of each class (label[k]) */
	int *nSV;		/* number of SVs for each class (nSV[k]) */
				/* nSV[0] + nSV[1] + ... + nSV[k-1] = l */
	/* XXX */
	int free_sv;		/* 1 if svm_model is created by svm_load_model*/
				/* 0 if svm_model is created by svm_train */
};


int print_null(const char *s,...) {return 0;}

static int (*info)(const char *fmt,...) = &printf;

//########################################################
int svm_get_svm_type(const struct svm_model *model);
int svm_get_nr_class(const struct svm_model *model);
double svm_get_svr_probability(const struct svm_model *model);
void svm_get_labels(const struct svm_model *model, int *label);
double svm_predict(const struct svm_model *model, const struct svm_node *x);
double svm_predict_values(const struct svm_model *model, const struct svm_node *x, double* dec_values);
static double k_function(const struct svm_node *x, const struct svm_node *y, const struct svm_parameter param);
static double sigmoid_predict(double decision_value, double A, double B);
struct svm_model *svm_load_model(const char *model_file_name);
bool read_model_header(FILE *fp, struct svm_model* model);
int svm_check_probability_model(const struct svm_model *model);
void svm_free_and_destroy_model(struct svm_model **model_ptr_ptr);
void svm_free_model_content(struct svm_model *model_ptr);
static double dot(const struct svm_node *px, const struct svm_node *py);
static inline double powi(double base, int times);
void exit_with_help();
double predict(double par[]);
static char* readline(FILE *input);
void exit_input_error(int line_num);

//######################################################## 


struct svm_node *x;
int max_nr_attr = 64;

struct svm_model* model;
int predict_probability=0;

static char *line = NULL;
static int max_line_len;

/*###################################################################################################################*/

pthread_t thread_id[17];
pthread_mutex_t lock;

void *pcap_loop_wrapper();
void *last_hp();
void *detect_states();
void *serror_rate();
void *same_srv_rate();
void *diff_srv_rate();
void *srv_diff_host_rate();
void *dst_host_count();
void *dst_host_same_src_port_rate();
void *dst_host_srv_serror_rate();
void *dst_host_rerror_rate();
void *dst_host_srv_rerror_rate();
//void *printing();
void *websocket();
void *insect();
void *insectVariables();
void *makecopy();
void web_alert();
int check();

float por_srv_rerror=-1;
float por_rerror_rate=-1;
float por_srv_serror=-1;
float por_cant_dstsrc_srv=-1;
int cant_dst_host=-1;
float por_srv_diff_host_rate=-1;
float por_diffsrv_rate=-1;
float por_srv_rate=-1;
float por_serror_rate=-1;

struct fivetuple
{
	char s_IP[INET_ADDRSTRLEN];
    char d_IP[INET_ADDRSTRLEN];
    //char* s_IP=(char*)malloc(sizeof(char) * INET_ADDRSTRLEN);
    //char* d_IP=(char*)malloc(sizeof(char) * INET_ADDRSTRLEN);
	int s_port;
	int d_port;
	int prot;
  	int cant_mensajes;
	int urgent;
	int timestamp;
	int estado; 		//1= normal; 2=SYN error, 3=REJ error.
	int last_timestamp;
	int byte_count;		//bytes intercambiados. Si es 0, apenas se ha establecido la conexion o se esta estableciendo. 
	int last_flag;		//1=FIN, 2=RST
	
};

double state_dec;

int cant_conversation=0;
int index_hp=0;
int dataLength = 0;
int timestamp_pc;
int hp_listo=0;
int indicador=0;
int cant_copia=0;
int indicadorts=0;
int indicadorhp=0;

int paso_limite=0;
int paso_limite_copia=0;

struct fivetuple arr_fivetuple[64000];

struct fivetuple arr_copia[64000];

struct fivetuple arr_ft_ts[64000];// 2 segundos

struct fivetuple arr_ft_hp[105];//100 paquete


void packet_manager(u_char *arg,const struct pcap_pkthdr* pkthdr,const u_char* packet)
{
	//sleep(1);
	pthread_mutex_lock(&lock);

	const struct ether_header *eptr; 							   //se crea un puntero a una estructura de header ethernet
	eptr = (struct ether_header *) packet;                                                     //se apunta a la parte del paquete (packet)
												   //donde se encuentra el header ethernet
												   //Se castea el packet a la estructura
												   //Para extraer el header
	const struct ip *ipc;
												   //ntohs() sirve para convertir el tipo
												   //de orden desde network byte a host byte
												   //(asuntos de compatibilidad)
												   //if (ntohs(eptr->ether_type) == ETHERTYPE_IP) {...} 
                                                                                                   //Se recomienda comprobar tipo de paquete capa 3
												   //Se revisa si es tipo IP
												   //Viene de #define	ETHERTYPE_IP	0x0800		/* IP protocol */

	ipc = (struct ip*)(packet + sizeof(struct ether_header)); 				   //Manera correcta de hacer lo que viene, se castea a
												   //la estructura usada para usarlo mas adelante
	 											   //Como packet es un puntero, se le suma
											           //el valor del tamano de ether_header para unicarlo

												   //Ojo: actualmente inet_ntoa esta descontinuado
											           //y se usa inet_ntop(), que ademas es compatible con hilos
	
	char sourceIp[INET_ADDRSTRLEN];
    char destIp[INET_ADDRSTRLEN];

	inet_ntop(AF_INET, &(ipc->ip_src), sourceIp, INET_ADDRSTRLEN);                              //INET_ADDRSTRLEN es un tamano adecuado para la IP
    	inet_ntop(AF_INET, &(ipc->ip_dst), destIp, INET_ADDRSTRLEN);
	
	

												     /*
								                                     //Estas son las maneras antiguas de hacerlo. Hacen lo mismo
                               	             							     //Se obtiene la direccion IP en un formato legible para el humano	
                                                                                                     char *ip_source=inet_ntoa(ipc->ip_src); 
												     char *ip_dest=inet_ntoa(ipc->ip_dst); //es decir, formaro ASCII
												     //Con -> se accede a un miembro de la estructura
												     //que es un puntero a la estructura.
												     */
	
	int prot_ip=ipc->ip_p;	                                                                     //Se obtiene un numero representando el protocolo
        u_int sourcePort, destPort;
	                                                                                               //capa 4 encapsulado en IP (1 para ICMP, 6 para TCP, 17 para UDP, etc).

	                                                                                             //char* payload= packet+sizeof(struct ether_header *)+ipc−>ip_len;	//Por siacaso, el payload
	int URG_flag;
	int ACK_flag;
	int PUSH_flag;
	int RST_flag;
	int SYN_flag;
	int FIN_flag;

	
        if (prot_ip == IPPROTO_TCP) {                                                          	     //Es importante verificar que tipo de paquete es
										                     //antes de procesarlo. (Como se obtuvo con int prot_ip=ipc->ip_p;)
										                     //Esto viene de #define	IPPROTO_TCP		6		/* tcp */		

	
		const struct tcphdr* tcpHeader=NULL;
		tcpHeader = (struct tcphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));  //Apuntamos a la parte correcta del packet
					                                                                 // y casteamos a estructura
			    
		sourcePort = ntohs(tcpHeader->source);
		destPort = ntohs(tcpHeader->dest);

		URG_flag=tcpHeader->urg;
		ACK_flag=tcpHeader->ack;
		PUSH_flag=tcpHeader->psh;
		RST_flag=tcpHeader->rst;
		SYN_flag=tcpHeader->syn;
		FIN_flag=tcpHeader->fin;
		dataLength = pkthdr->len - (sizeof(struct ether_header) + sizeof(struct ip) + sizeof(struct tcphdr));
		
	
	}	
	 else if (prot_ip == IPPROTO_UDP) {  
	
       		const struct udphdr* udpHeader=NULL;
		udpHeader = (struct udphdr*)(packet + sizeof(struct ether_header) + sizeof(struct ip));  //Apuntamos a la parte correcta del packet
					                                                                 // y casteamos a estructura
		  	 	    
		sourcePort = ntohs(udpHeader->source);
		destPort = ntohs(udpHeader->dest);
		
	
	}
	else if (prot_ip == IPPROTO_ICMP){

	}
	

	
	int i;
	
	if(prot_ip== IPPROTO_TCP){
		int bingo=0;
		
		
			
		if (cant_conversation || paso_limite){
		
			for (i=0; i<(( (!paso_limite) *cant_conversation)+(64000*paso_limite)); ++i){

				if( ( ( !strcmp(sourceIp, arr_fivetuple[i].s_IP) && !strcmp(destIp, arr_fivetuple[i].d_IP)) || ( !strcmp(sourceIp, arr_fivetuple[i].d_IP) && !strcmp(destIp, arr_fivetuple[i].s_IP)) ) && 
					( (sourcePort==arr_fivetuple[i].s_port && destPort==arr_fivetuple[i].d_port) || (sourcePort==arr_fivetuple[i].d_port && destPort==arr_fivetuple[i].s_port) ) 
					&& prot_ip==arr_fivetuple[i].prot){

					arr_fivetuple[i].cant_mensajes++;
					
					if(prot_ip== IPPROTO_TCP){
						
						if(URG_flag){
							arr_fivetuple[i].urgent++;
						}
						
						
						if(FIN_flag){
						arr_fivetuple[i].last_flag=1;

						}
						else if(RST_flag){

							arr_fivetuple[i].last_flag=2;
						}
						
								
					}
					
					arr_fivetuple[i].last_timestamp=pkthdr->ts.tv_sec;
					arr_fivetuple[i].byte_count+=dataLength;
					
			
					bingo=1;
					
				}
			}
		}	
			
		if (!bingo){
			strcpy(arr_fivetuple[cant_conversation].s_IP, sourceIp);
			strcpy(arr_fivetuple[cant_conversation].d_IP, destIp);
			
			arr_fivetuple[cant_conversation].s_port=sourcePort;
			arr_fivetuple[cant_conversation].d_port=destPort;
			arr_fivetuple[cant_conversation].prot=prot_ip;
			arr_fivetuple[cant_conversation].cant_mensajes=1;
			arr_fivetuple[cant_conversation].timestamp=pkthdr->ts.tv_sec;
			arr_fivetuple[cant_conversation].last_timestamp=pkthdr->ts.tv_sec;				
			arr_fivetuple[cant_conversation].byte_count=dataLength;				
	
			if(prot_ip== IPPROTO_TCP)
			{
				if(URG_flag){
					arr_fivetuple[cant_conversation].urgent=1;
				}
				else{
					arr_fivetuple[cant_conversation].urgent=0;
				}

				
				
				if(FIN_flag){
					arr_fivetuple[cant_conversation].last_flag=1;

				}
				else if(RST_flag){

					arr_fivetuple[cant_conversation].last_flag=2;
				}
				else{
					arr_fivetuple[cant_conversation].last_flag=0;
				}
				

				arr_fivetuple[cant_conversation].estado=1;

			}
			else{
				
				arr_fivetuple[cant_conversation].urgent=-1;
				arr_fivetuple[cant_conversation].estado=-1;
			}

		
			cant_conversation++;
			
			if(cant_conversation==64000){
				
				paso_limite=1;
				cant_conversation=0;
				}
			
			
			
		}

	 }
			
	pthread_mutex_unlock(&lock);
	
}
int k;
void *last_ts_buffer(){
	
	while(1){
		
		pthread_mutex_lock(&lock);
		if((indicador==2 || indicador==3) && !indicadorts){
		//printf("entra a ts\n");
		
		memset(arr_ft_ts, 0, sizeof arr_ft_ts);

		timestamp_pc=(int)time(NULL);
		int j;
		k=0;
		if(!paso_limite_copia){
		for(j=0; j<cant_copia; j++){

			if( (timestamp_pc-arr_copia[j].timestamp)<=2){
		
				arr_ft_ts[k]=arr_copia[j];		
				k++;
			}

		}
	indicador++;
		//printf("entra indicador ts %d\n",indicador);

	indicadorts=1;
	}
	else if(paso_limite_copia){
				
				int j;
		//copiar los cambios de conexiones viejas
		for(j=0; j<64000; j++){
			
			if( (timestamp_pc-arr_copia[j].timestamp)<=2){
		
				arr_ft_ts[k]=arr_copia[j];		
				k++;
			}
			
			}
			
			indicador++;
		//printf("entra indicador ts %d\n",indicador);

	indicadorts=1;
		}
}


	
	pthread_mutex_unlock(&lock);
}
}



pcap_t *handle;// indentificador de sesion

 int main(int argc, char *argv[])
 {

	if(argc != 3){
		printf("Ejecutar con formato './sniffer <modo> <modelo>'. Modo es D (deteccion) o E (entrenamiento) \n");
		
		
		}
		else{
			
			
			char * dev,errbuf[PCAP_ERRBUF_SIZE];// SE UTILIZAR ESTA CADENA, POR SI LA CONEXION FALLA.
		   bpf_u_int32 mask;//mascara de red
		   bpf_u_int32 net; //nuestra ip
		   struct pcap_pkthdr header;//el encabezado que pcap nos da
		   const u_char *packet;//paquete real

		   dev= pcap_lookupdev(errbuf);//En el caso que la conexion falle, se almacenara un mensaje de    error en errbuf. 
		   if(dev==NULL){//EN el caso de que no encuentre ningun dipositivo conectado, terminara el programa.
		   fprintf(stderr,"No se pudo encontrar el dispositivo predeterminado:%s\n",errbuf);
		   return (2);
		   }
		   printf("Dispositivo: %s\n",dev);//imprimer la tarjeta de red que se encuentra conectado.
		   handle=pcap_open_live(dev, BUFSIZ,1,-1,errbuf);
		   if(handle==NULL){
		   fprintf(stderr,"No se pudo abrir el dispositivo %s:%s\n",dev,errbuf);//te imprimer el puerto   por donde no pudo comunicarse.
		   return (2);
		   }
			
			/*##########################################################################################################*/
			/*SVM*/
			 
			FILE *input;
			int i;
			input = fopen(argv[2],"r");
			if(input == NULL)
			{
				fprintf(stderr,"no se puede abrir el archivo de entrada %s\n",argv[2]);
				exit(1);
			}
			
			if((model=svm_load_model(argv[2]))==0)
			{
				fprintf(stderr,"no se puede abrir el archivo modelo %s\n",argv[i+1]);
				exit(1);
			}

			x = (struct svm_node*) malloc(max_nr_attr*sizeof(struct svm_node));
			
			if(predict_probability)
			{
				if(svm_check_probability_model(model)==0)
				{
					fprintf(stderr,"El modelo no soporta estimaciones de probabilidad\n");
					exit(1);
				}
			}
			else
			{
				if(svm_check_probability_model(model)!=0)
					info("El modelo soporta estimaciones de probabilidad, pero estan deshabilitadas en prediccion\n");
			}
			/*
			double par[]={0, 0, 0, 0, 100, 1, 0, 50, 1};
			
			double resultado=predict(par);
			printf("Resultado: %f\n", resultado);
			*/
		/*	
			arr0: 0.000000
		arr1: 0.000000
		arr2: 0.000000
		arr3: 0.000000
		arr4: 100.000000
		arr5: 1.000000
		arr6: 0.000000
		arr7: 100.000000
		arr8: 1.000000
		*/
		/*##########################################################################################################*/		
	
	
	if(*argv[1]=='D'){
			
	//Crear hilos
	pthread_mutex_init(&lock, NULL);
	
	pthread_create(&thread_id[0], NULL, pcap_loop_wrapper, NULL);
	
	while(cant_conversation==0){
	}
	

	pthread_create(&thread_id[1], NULL, last_ts_buffer, NULL);
	pthread_create(&thread_id[2], NULL, detect_states, NULL);
	pthread_create(&thread_id[3], NULL, serror_rate, NULL);
	pthread_create(&thread_id[4], NULL, same_srv_rate, NULL);
	pthread_create(&thread_id[5], NULL, diff_srv_rate, NULL);
	pthread_create(&thread_id[6], NULL, srv_diff_host_rate, NULL);
	pthread_create(&thread_id[7], NULL, dst_host_count, NULL);
	pthread_create(&thread_id[8], NULL, dst_host_same_src_port_rate, NULL);
	pthread_create(&thread_id[9], NULL, dst_host_srv_serror_rate, NULL);
	pthread_create(&thread_id[10], NULL, dst_host_rerror_rate, NULL);
	pthread_create(&thread_id[11], NULL, dst_host_srv_rerror_rate, NULL);
	pthread_create(&thread_id[12], NULL, websocket, NULL);
	pthread_create(&thread_id[13], NULL, last_hp, NULL);
	pthread_create(&thread_id[14], NULL, makecopy, NULL);
	pthread_create(&thread_id[15], NULL, insect, NULL);
	pthread_create(&thread_id[16], NULL, get_telephone, NULL);
	
	

	
	pthread_join(thread_id[0], NULL);
	pthread_join(thread_id[1], NULL);
	pthread_join(thread_id[2], NULL);
	pthread_join(thread_id[3], NULL);
	pthread_join(thread_id[4], NULL);
	pthread_join(thread_id[5], NULL);
	pthread_join(thread_id[6], NULL);
	pthread_join(thread_id[7], NULL);
	pthread_join(thread_id[8], NULL);
	pthread_join(thread_id[9], NULL);
	pthread_join(thread_id[10], NULL);
	pthread_join(thread_id[11], NULL);
	pthread_join(thread_id[12], NULL);
	pthread_join(thread_id[13], NULL);
	pthread_join(thread_id[14], NULL);
	pthread_join(thread_id[15], NULL);
	pthread_join(thread_id[16], NULL);

	
	pthread_mutex_destroy(&lock);

	

  //------------------------------
    
  // packet=pcap_next(handle,& header);//estoy selecionado un paquete
  //printf("Conecto un paquete con una longitud de [% d]\n", header.len);//imprime la logitud de los paquetes.
   //pcap_loop(handle, -1, packet_manager, NULL); 

	}
	else if(*argv[1]=='E'){
		
		pthread_mutex_init(&lock, NULL);
		
		pthread_create(&thread_id[0], NULL, pcap_loop_wrapper, NULL);
		
		while(cant_conversation==0){
		}
		pthread_create(&thread_id[1], NULL, last_ts_buffer, NULL);
	pthread_create(&thread_id[2], NULL, detect_states, NULL);
	pthread_create(&thread_id[3], NULL, serror_rate, NULL);
	pthread_create(&thread_id[4], NULL, same_srv_rate, NULL);
	pthread_create(&thread_id[5], NULL, diff_srv_rate, NULL);
	pthread_create(&thread_id[6], NULL, srv_diff_host_rate, NULL);
	pthread_create(&thread_id[7], NULL, dst_host_count, NULL);
	pthread_create(&thread_id[8], NULL, dst_host_same_src_port_rate, NULL);
	pthread_create(&thread_id[9], NULL, dst_host_srv_serror_rate, NULL);
	pthread_create(&thread_id[10], NULL, dst_host_rerror_rate, NULL);
	pthread_create(&thread_id[11], NULL, dst_host_srv_rerror_rate, NULL);
	pthread_create(&thread_id[12], NULL, insectVariables, NULL);
	pthread_create(&thread_id[13], NULL, last_hp, NULL);
	pthread_create(&thread_id[14], NULL, makecopy, NULL);
	
	

	
	pthread_join(thread_id[0], NULL);
	pthread_join(thread_id[1], NULL);
	pthread_join(thread_id[2], NULL);
	pthread_join(thread_id[3], NULL);
	pthread_join(thread_id[4], NULL);
	pthread_join(thread_id[5], NULL);
	pthread_join(thread_id[6], NULL);
	pthread_join(thread_id[7], NULL);
	pthread_join(thread_id[8], NULL);
	pthread_join(thread_id[9], NULL);
	pthread_join(thread_id[10], NULL);
	pthread_join(thread_id[11], NULL);
	pthread_join(thread_id[12], NULL);
	pthread_join(thread_id[13], NULL);
	pthread_join(thread_id[14], NULL);
	
	pthread_mutex_destroy(&lock);
		
		}
		
	svm_free_and_destroy_model(&model);
	free(x);
	fclose(input);
	
	pcap_close(handle);//cierra la sesion de tomar paquete
		
	}

   
   
   return(0);	
 }


void *pcap_loop_wrapper(){

	pcap_loop(handle, -1, packet_manager, NULL); 
}


void *last_hp(){
	
	while(1){
		pthread_mutex_lock(&lock);
	if((indicador==2 || indicador==3) && !indicadorhp){
	
	//printf("entra a hp\n");
	arr_ft_hp[index_hp]=arr_copia[cant_copia-1];
	
	index_hp++;

	if(index_hp==100){
		index_hp=0;
		hp_listo=1;
	}
	
	if(hp_listo==1){
		
		
		int i;
		int m=0;
		
		if(!paso_limite_copia){
		for(i=cant_copia-100; i<cant_copia; i++){
			arr_ft_hp[m]=arr_copia[i];
			m++;
			}
		}
		
		if(paso_limite_copia){
			
			for(i=0; i<cant_copia; i++){
			arr_ft_hp[m]=arr_copia[i];
			m++;
			}
			
			
			for(i=64000-(100-cant_copia); i<64000; i++){
			arr_ft_hp[m]=arr_copia[i];
			m++;
			}
			
			}
		
		
	}
	indicador++;
	//printf("entra indicador hp %d\n",indicador);
indicadorhp=1;



}


pthread_mutex_unlock(&lock);
}				
}

void *detect_states(){
	int m;
	while(1){
	
		pthread_mutex_lock(&lock);
		if(indicador==0){
		
		for(m=0; m<(( (!paso_limite) *cant_conversation)+(64000*paso_limite)); m++){
			timestamp_pc=(int)time(NULL);
		
			if( ( ( (timestamp_pc)-(arr_fivetuple[m].last_timestamp) )>=300) && arr_fivetuple[m].byte_count<=26){

				arr_fivetuple[m].estado= 2;					

			}
			else if(arr_fivetuple[m].byte_count<=26 && arr_fivetuple[m].last_flag==1){
				arr_fivetuple[m].estado= 2;	
			}
			else if(arr_fivetuple[m].byte_count<=26 && arr_fivetuple[m].last_flag==2){
				arr_fivetuple[m].estado= 3;	
			}
		
		}
		/**/
		
		indicador++;
		//printf("entra indicador detect %d\n",indicador);
	}
	
		pthread_mutex_unlock(&lock);
	}
}

void *serror_rate(){
float cant_serror_rate;
int cont;
	while(1){
		
		pthread_mutex_lock(&lock);
		if(indicador>=4 && por_serror_rate==-1){
		//printf("k: %d \n",k );
		if(k>0){
		cant_serror_rate=0;

		for(cont=0; cont<k; cont++){//Revisar que variable ray poner en este caso.
			
			if(arr_ft_ts[cont].estado==2 && !strcmp(arr_copia[cant_copia-1].d_IP, arr_ft_ts[cont].d_IP) ){
				cant_serror_rate++;
			}
			
		 }
		
		por_serror_rate=(float)(cant_serror_rate/(float)k)*100;
	}
	else{
		por_serror_rate=0;
		}
		indicador++;
		//printf("entra indicador 1 %d\n",indicador);
		}
		
	pthread_mutex_unlock(&lock);
}
}

///////////////////

void *same_srv_rate(){
float cant_srv_rate;
int cont;

while(1){
		
	pthread_mutex_lock(&lock);
	 if(indicador>=4 && por_srv_rate==-1){
		//printf("1\n");
		if(k>0){
		cant_srv_rate=0;
		
		for(cont=0; cont<k; cont++){
						
			if((arr_copia[cant_copia-1].d_port==arr_ft_ts[cont].d_port) && !strcmp(arr_copia[cant_copia-1].d_IP, arr_ft_ts[cont].d_IP) ){
			cant_srv_rate++;
			}		
			
		}
		por_srv_rate=(float)(cant_srv_rate/(float)k)*100;
	}
	else
	{
		por_srv_rate=0;
		}
		indicador++;	
		//printf("entra indicador 2 %d\n",indicador);
	}	
	
	pthread_mutex_unlock(&lock);
  }
}
/////////////////

void *diff_srv_rate(){
float cant_diffsrv_rate;
int cont;

while(1){
		
		pthread_mutex_lock(&lock);
		 if(indicador>=4 && por_diffsrv_rate==-1){
			 
			 if(k>0){
			 //	printf("2\n");
			cant_diffsrv_rate=0;
			
			for(cont=0; cont<k; cont++){
			
				if((arr_copia[cant_copia-1].d_port!=arr_ft_ts[cont].d_port) && !strcmp(arr_copia[cant_copia-1].d_IP, arr_ft_ts[cont].d_IP) ){
					cant_diffsrv_rate++;
				}
					
			}
			por_diffsrv_rate=(float)(cant_diffsrv_rate/(float)k)*100;
		}
		else{
			por_diffsrv_rate=0;
			}
			indicador++;
			//printf("entra indicador 3 %d\n",indicador);	
	  }	
	
	pthread_mutex_unlock(&lock);
}
}
///////////////////////

void *srv_diff_host_rate(){
float cant_srv_diff_host_rate;
int cont;

	while(1){
			
		pthread_mutex_lock(&lock);
		if(indicador>=4 && por_srv_diff_host_rate==-1){
			if(k>0){
		cant_srv_diff_host_rate=0;
		
		for(cont=0; cont<k; cont++){

			if((arr_copia[cant_copia-1].d_port==arr_ft_ts[cont].d_port) && strcmp(arr_copia[cant_copia-1].d_IP, arr_ft_ts[cont].d_IP)){
				cant_srv_diff_host_rate++;
			 }
		}
		
		por_srv_diff_host_rate=(float)(cant_srv_diff_host_rate/(float)k)*100;
	}
	else{
		por_srv_diff_host_rate=0;
		}
		indicador++;//aqui
		//printf("entra indicador 4 %d\n",indicador);
	}
	
	pthread_mutex_unlock(&lock);
}
}
//////////////////////////

void *dst_host_count(){
	int cant_dst_hosting;
	int cont;

	while(1){
		
		pthread_mutex_lock(&lock);
		if(indicador>=4 && cant_dst_host==-1){
			if(hp_listo==1){
			cant_dst_hosting=0;		

			 for(cont=0; cont<100;cont++){
				
					if(!strcmp(arr_ft_hp[cont].d_IP, arr_copia[cant_copia-1].d_IP) ){
						cant_dst_hosting++;
					}	
				 }	
				
			cant_dst_host=cant_dst_hosting;
			
		}else{
			cant_dst_host=0;
			}
		indicador++;
		//printf("entra indicador 5 %d\n",indicador);
		}
	
	pthread_mutex_unlock(&lock);
}

}
//////////////////////////

void *dst_host_same_src_port_rate(){
int cont;
float cant_dstsrc_srv;

while(1){
	
	pthread_mutex_lock(&lock);
	if(indicador>=4 && por_cant_dstsrc_srv==-1){
		if(hp_listo==1){
			cant_dstsrc_srv=0;
	
			for(cont=0; cont<100; cont++){
					
				if( (arr_ft_hp[cont].d_port == arr_copia[cant_copia-1].d_port) && (arr_ft_hp[cont].s_port == arr_copia[cant_copia-1].s_port) ){
					cant_dstsrc_srv++;		
				}	

			}
		
		por_cant_dstsrc_srv= (cant_dstsrc_srv/100)*100;
	}else{
			por_cant_dstsrc_srv=0;
			}
	 indicador++;
	 //printf("entra indicador 6 %d\n",indicador);
	}
	
	pthread_mutex_unlock(&lock);
}

}
///////////////////////////////////

void *dst_host_srv_serror_rate(){
int cont;
float cant_srv_serror;

while(1){
		
	pthread_mutex_lock(&lock);
	
	if(indicador>=4 && por_srv_serror==-1){
		if(hp_listo==1){
	cant_srv_serror=0;

		for(cont=0; cont<100; cont++){
			
			if( (arr_ft_hp[cont].d_port == arr_copia[cant_copia-1].d_port) && arr_ft_hp[cont].estado == 2){
				cant_srv_serror++;		
			}			
	  }
	  
		por_srv_serror= (cant_srv_serror/100)*100;
	}else{
			por_srv_serror=0;
			}	
		indicador++;
		//printf("entra indicador 7 %d\n",indicador);
	}
	
	pthread_mutex_unlock(&lock);
}
}
//////////////////////////////////
void *dst_host_rerror_rate(){
	int cont;
	float cant_rerror_rate;

	while(1){
		
		pthread_mutex_lock(&lock);
		
			if(indicador>=4 && por_rerror_rate==-1){
				if(hp_listo==1){

				cant_rerror_rate=0;

				for(cont=0; cont<100; cont++){

						if((!strcmp(arr_ft_hp[cont].d_IP, arr_copia[cant_copia-1].d_IP)) && (arr_ft_hp[cont].estado == 3)){
							cant_rerror_rate++;					
						}		
				 }
				por_rerror_rate=(cant_rerror_rate/100)*100;	
			}else{
			por_rerror_rate=0;
			}		
			indicador++;	
			//printf("entra indicador 8 %d\n",indicador);
		}
	
	pthread_mutex_unlock(&lock);
}

}
/////////////////////////////////////

void *dst_host_srv_rerror_rate(){
	
int cont;
	float cant_srv_rerror;
	
	while(1){
		
		pthread_mutex_lock(&lock);
			
			if(indicador>=4 && por_srv_rerror==-1){
				
			if(hp_listo==1){
			cant_srv_rerror=0;
			
			for(cont=0; cont<100; cont++){

				if( (arr_ft_hp[cont].d_port == arr_copia[cant_copia-1].d_port) && (arr_ft_hp[cont].estado == 3)){
					cant_srv_rerror++;
				}		
		    }
			por_srv_rerror= (cant_srv_rerror/100)*100;
		}else{
			por_srv_rerror=0;
			}	
		indicador++;
		//printf("entra indicador 9 %d\n",indicador);
		}

pthread_mutex_unlock(&lock);
}
}


/*###########################################################################################################################*/

char bufG[300];
char bufc[50];
//char buf[1000000];
//float arr1[]={0,0,0,0,0,0,0,0,0,0,0}; //cambio ####################################################################################
double arr1[]={0,0,0,0,0,0,0,0,0,0,0};

float punto=0;
int contador;


#define TX_BUFFER_BYTES (1000000)

struct payload

{

	unsigned char data[LWS_SEND_BUFFER_PRE_PADDING + TX_BUFFER_BYTES + LWS_SEND_BUFFER_POST_PADDING];

	size_t len;

} to_send_payload;



static int my_callback( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )

{

	switch( reason )

	{

		case LWS_CALLBACK_RECEIVE:
			
			lwsl_notice("Connection established\n");
			
			lws_callback_on_writable_all_protocol( lws_get_context( wsi ), lws_get_protocol( wsi ) );
			
			break;


		
		case LWS_CALLBACK_SERVER_WRITEABLE:
		
			
			
			punto=arr1[0];
			gcvt(punto,8,bufc);
			strcpy(bufG,"{\"por_serror_rate\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[1];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_srv_rate\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[2];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_diffsrv_rate\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[3];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_srv_diff_host_rate\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[4];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"cant_dst_host\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[5];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_cant_dstsrc_srv\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[6];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_srv_serror\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[7];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_rerror_rate\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,",");
			punto=arr1[8];
			gcvt(punto,8,bufc);
			strcat(bufG,"\"por_srv_rerror\":");
			strcat(bufG,bufc);
			memset(bufc, 0, sizeof(bufc));
			strcat(bufG,"}");
			//printf("%s\n",bufG);	
			
			memcpy( &to_send_payload.data[LWS_SEND_BUFFER_PRE_PADDING], bufG,strlen(bufG));
			to_send_payload.len=strlen(bufG);
			lws_callback_on_writable_all_protocol( lws_get_context( wsi ), lws_get_protocol( wsi ) );
			lws_write( wsi, &to_send_payload.data[LWS_SEND_BUFFER_PRE_PADDING], to_send_payload.len, LWS_WRITE_TEXT);
			memset(bufG, 0, sizeof(bufG));
		
			sleep(1);
			
			break;



		default:

			break;

	}



	return 0;

}



enum protocols

{

	PROTOCOL_HTTP = 0,

	PROTOCOL_MY

};



static struct lws_protocols protocols[] =

{

	{

		"",

		my_callback,

		0,

		TX_BUFFER_BYTES,

	},

	{ NULL, NULL, 0, 0 } /* terminator */

};


		
void *websocket(){
	
	struct lws_context_creation_info info;

	memset( &info, 0, sizeof(info) );



	info.port = 8000;

	info.protocols = protocols;

	info.gid = -1;

	info.uid = -1;



	struct lws_context *context = lws_create_context( &info );

	

	while( 1 )

	{
		//pthread_mutex_lock(&lock)
			
		lws_service( context, /* timeout_ms = */ 1000000);
		
	//pthread_mutex_unlock(&lock);
	}

	lws_context_destroy( context );

	

	return 0;
}



/*###########################################################################################################################*/
/*base de datos*/

int cant_conversation_history=0;
void *insect(){
	
while(1){
pthread_mutex_lock(&lock);


int x=0;
mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *doc;
//char bufbd[50];


mongoc_init ();
client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "connections");
if(!paso_limite){
while(x<cant_conversation_history){
	   
	   doc = bson_new ();
	   BSON_APPEND_UTF8 (doc, "R","del");
	   
	   if (!mongoc_collection_delete_one (
			 collection, doc, NULL, NULL, &error)) {
		  fprintf (stderr, "Delete failed: %s\n", error.message);
	   }

	   bson_destroy (doc);
	   x++;

}

x=0;
cant_conversation_history= cant_conversation;//arreglar #####################################################################################################################################
}else{
	while(x<cant_conversation_history){
	   
	   doc = bson_new ();
	   BSON_APPEND_UTF8 (doc, "R","del");
	   
	   if (!mongoc_collection_delete_one (
			 collection, doc, NULL, NULL, &error)) {
		  fprintf (stderr, "Delete failed: %s\n", error.message);
	   }

	   bson_destroy (doc);
	   x++;

}

x=0;
cant_conversation_history= 64000;
	
	
	}
while(x<cant_conversation_history){
	
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arr_fivetuple[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arr_fivetuple[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arr_fivetuple[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arr_fivetuple[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arr_fivetuple[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arr_fivetuple[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arr_fivetuple[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arr_fivetuple[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arr_fivetuple[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arr_fivetuple[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arr_fivetuple[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arr_fivetuple[x].last_flag);
		BSON_APPEND_UTF8 (doc, "R","del");
		
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		
		bson_destroy (doc);
		x++;
	}
	
mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();

pthread_mutex_unlock(&lock);
sleep(10);
}
}


/*
int cant_conversation_history=0;

void *insect(){
	
while(1){
pthread_mutex_lock(&lock);


int x=0;
mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *doc;
//char bufbd[50];


mongoc_init ();
client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "connections");
		
while(x<cant_conversation_history){
	   
	   doc = bson_new ();
	   BSON_APPEND_UTF8 (doc, "R","del");
	   
	   if (!mongoc_collection_delete_one (
			 collection, doc, NULL, NULL, &error)) {
		  fprintf (stderr, "Delete failed: %s\n", error.message);
	   }

	   bson_destroy (doc);
	   x++;

}


x=0;
cant_conversation_history= cant_conversation;//arreglar #####################################################################################################################################

while(x<cant_conversation_history){
	
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arr_fivetuple[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arr_fivetuple[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arr_fivetuple[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arr_fivetuple[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arr_fivetuple[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arr_fivetuple[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arr_fivetuple[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arr_fivetuple[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arr_fivetuple[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arr_fivetuple[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arr_fivetuple[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arr_fivetuple[x].last_flag);
		BSON_APPEND_UTF8 (doc, "R","del");
		
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		
		bson_destroy (doc);
		x++;
	}
	
mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();

pthread_mutex_unlock(&lock);
sleep(10);
}
}
*/


/*
int cant_conversation_history=0;
struct fivetuple arrccopiabd[64000];
int valorConversacion=0;

void *insect(){
	
int x=0,y=0,i=0;
mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *doc;

mongoc_init ();
client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "connections");

if(!paso_limite){
y=cant_conversation;
	
for(i=0; i<y; i++){
	arrccopiabd[i]=arr_fivetuple[i];
}	
x=0;
while(x<cant_conversation_history){
	   
	   doc = bson_new ();
	   BSON_APPEND_UTF8 (doc, "R","del");
	   
	   if (!mongoc_collection_delete_one (
			 collection, doc, NULL, NULL, &error)) {
		  fprintf (stderr, "Delete failed: %s\n", error.message);
	   }

	   bson_destroy (doc);
	   x++;

}

cant_conversation_history= y;
x=0;

while(x<cant_conversation_history){
	
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arrccopiabd[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arrccopiabd[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arrccopiabd[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arrccopiabd[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arrccopiabd[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arrccopiabd[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arrccopiabd[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arrccopiabd[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arrccopiabd[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arrccopiabd[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arrccopiabd[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arrccopiabd[x].last_flag);
		BSON_APPEND_UTF8 (doc, "R","del");
		
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		
		bson_destroy (doc);
		x++;
	}
}

if(paso_limite){
	
x=0;
while(x<64000){	   
	   doc = bson_new ();
	   BSON_APPEND_UTF8 (doc, "R","del");
	   
	   if (!mongoc_collection_delete_one (
			 collection, doc, NULL, NULL, &error)) {
		  fprintf (stderr, "Delete failed: %s\n", error.message);
	   }
	   bson_destroy (doc);
	   x++;
}


if(cant_conversation_history<cant_conversation){
valorConversacion=cant_conversation;
for(x=cant_conversation_history; x<valorConversacion; x++){
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arrccopiabd[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arrccopiabd[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arrccopiabd[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arrccopiabd[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arrccopiabd[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arrccopiabd[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arrccopiabd[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arrccopiabd[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arrccopiabd[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arrccopiabd[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arrccopiabd[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arrccopiabd[x].last_flag);
		BSON_APPEND_UTF8 (doc, "C","del");
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		bson_destroy (doc);
		}
		cant_conversation_history=valorConversacion;	
		}else{
		for(x=0; x<valorConversacion; x++){
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arrccopiabd[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arrccopiabd[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arrccopiabd[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arrccopiabd[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arrccopiabd[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arrccopiabd[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arrccopiabd[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arrccopiabd[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arrccopiabd[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arrccopiabd[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arrccopiabd[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arrccopiabd[x].last_flag);
		BSON_APPEND_UTF8 (doc, "C","del");
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		bson_destroy (doc);
		}
		for(x=cant_conversation_history; x<64000; x++){
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arrccopiabd[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arrccopiabd[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arrccopiabd[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arrccopiabd[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arrccopiabd[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arrccopiabd[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arrccopiabd[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arrccopiabd[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arrccopiabd[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arrccopiabd[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arrccopiabd[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arrccopiabd[x].last_flag);
		BSON_APPEND_UTF8 (doc, "C","del");
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		bson_destroy (doc);
		}
		cant_conversation_history=valorConversacion;	
			
			}
					
		for(i=0; i<64000; i++){
			arrccopiabd[i]=arr_fivetuple[i];
		}
		
		x=0;
		while(x<64000){
	
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_UTF8 (doc, "s_IP",arrccopiabd[x].s_IP);
		BSON_APPEND_UTF8 (doc, "d_IP",arrccopiabd[x].d_IP);
		BSON_APPEND_INT32 (doc, "s_port",arrccopiabd[x].s_port);
		BSON_APPEND_INT32 (doc, "d_port",arrccopiabd[x].d_port);
		BSON_APPEND_INT32 (doc, "prot",arrccopiabd[x].prot);
		BSON_APPEND_INT32 (doc, "cant_mensajes",arrccopiabd[x].cant_mensajes);
		BSON_APPEND_INT32 (doc, "urgent",arrccopiabd[x].urgent);
		BSON_APPEND_INT32 (doc, "timestamp",arrccopiabd[x].timestamp);
		BSON_APPEND_INT32 (doc, "estado",arrccopiabd[x].estado);
		BSON_APPEND_INT32 (doc, "last_timestamp",arrccopiabd[x].last_timestamp);
		BSON_APPEND_INT32 (doc, "byte_count",arrccopiabd[x].byte_count);
		BSON_APPEND_INT32 (doc, "last_flag",arrccopiabd[x].last_flag);
		BSON_APPEND_UTF8 (doc, "R","del");
		
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		
		bson_destroy (doc);
		x++;
		}
		


}
mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();


}
*/

void *insectVariables(){
//int x=0;
while(1){
	pthread_mutex_lock(&lock);
	//sleep(1);
mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *doc;
//char bufbd[50];


mongoc_init ();
client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "variables");

	
		doc = bson_new ();
		bson_oid_init (&oid, NULL);
		BSON_APPEND_DOUBLE (doc, "por_serror_rate",arr1[0]);
		BSON_APPEND_DOUBLE (doc, "por_srv_rate",arr1[1]);
		BSON_APPEND_DOUBLE (doc, "por_diffsrv_rate",arr1[2]);
		BSON_APPEND_DOUBLE (doc, "por_srv_diff_host_rate",arr1[3]);
		BSON_APPEND_INT32 (doc, "cant_dst_host",arr1[4]);
		BSON_APPEND_DOUBLE (doc, "por_cant_dstsrc_srv",arr1[5]);
		BSON_APPEND_DOUBLE (doc, "por_srv_serror",arr1[6]);
		BSON_APPEND_DOUBLE (doc, "por_rerror_rate",arr1[7]);
		BSON_APPEND_DOUBLE (doc, "por_srv_rerror",arr1[8]);
		
		if (!mongoc_collection_insert_one (
			   collection, doc, NULL, NULL, &error)) {
			fprintf (stderr, "%s\n", error.message);
		}
		
		bson_destroy (doc);
	
mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();

pthread_mutex_unlock(&lock);
sleep(1);
}
}

int llamar=0;

void *makecopy(){
	double prediccion;
	double prediccion_sum=0;
	//lwsl_notice("entra a copy\n");
	while(1){
		pthread_mutex_lock(&lock);
	while(cant_conversation!=cant_copia && indicador==1){
		
		if(!paso_limite){
			
			int i;
		//copiar los cambios de conexiones viejas
		for(i=0; i<cant_copia; i++){
			arr_copia[i]=arr_fivetuple[i];
			}
		//copiar conexion vieja
		arr_copia[cant_copia]=arr_fivetuple[cant_copia];
		cant_copia++;
		indicador++;
			//printf("entra indicador main %d\n",indicador);
			
			}
		
		else if(paso_limite){
			int i;
		//copiar los cambios de conexiones viejas
		for(i=0; i<cant_copia; i++){
			
			if( ( ( !strcmp(arr_copia[i].s_IP, arr_fivetuple[i].s_IP) && !strcmp(arr_copia[i].d_IP, arr_fivetuple[i].d_IP)) || ( !strcmp(arr_copia[i].s_IP, arr_fivetuple[i].d_IP) && !strcmp(arr_copia[i].d_IP, arr_fivetuple[i].s_IP)) ) && 
					( (arr_copia[i].s_port==arr_fivetuple[i].s_port && arr_copia[i].d_port==arr_fivetuple[i].d_port) || (arr_copia[i].s_port==arr_fivetuple[i].d_port && arr_copia[i].d_port==arr_fivetuple[i].s_port) ) 
					&& arr_copia[i].prot==arr_fivetuple[i].prot)
					{
						arr_copia[i]=arr_fivetuple[i];
						
						}
			
			}
			arr_copia[cant_copia]=arr_fivetuple[cant_copia];
			cant_copia++;
			indicador++;
			
			
			}
			
			else if(paso_limite && paso_limite_copia){
				
				int i;
		//copiar los cambios de conexiones viejas
		for(i=0; i<64000; i++){
			
			if( ( ( !strcmp(arr_copia[i].s_IP, arr_fivetuple[i].s_IP) && !strcmp(arr_copia[i].d_IP, arr_fivetuple[i].d_IP)) || ( !strcmp(arr_copia[i].s_IP, arr_fivetuple[i].d_IP) && !strcmp(arr_copia[i].d_IP, arr_fivetuple[i].s_IP)) ) && 
					( (arr_copia[i].s_port==arr_fivetuple[i].s_port && arr_copia[i].d_port==arr_fivetuple[i].d_port) || (arr_copia[i].s_port==arr_fivetuple[i].d_port && arr_copia[i].d_port==arr_fivetuple[i].s_port) ) 
					&& arr_copia[i].prot==arr_fivetuple[i].prot)
					{
						arr_copia[i]=arr_fivetuple[i];
						
						}
			
			}
			arr_copia[cant_copia]=arr_fivetuple[cant_copia];
			cant_copia++;
			indicador++;
				
				}
		

		
		}

		if(indicador==13){
			
			arr1[0]=por_serror_rate;
			arr1[1]=por_srv_rate;
			arr1[2]=por_diffsrv_rate;
			arr1[3]=por_srv_diff_host_rate;
			arr1[4]=cant_dst_host;
			arr1[5]=por_cant_dstsrc_srv;
			arr1[6]=por_srv_serror;
			arr1[7]=por_rerror_rate;
			arr1[8]=por_srv_rerror;
			/*###########################################################################################*/
			/*SVM*/
	
			printf("arr0: %f\n", arr1[0]);
			printf("arr1: %f\n", arr1[1]);
			printf("arr2: %f\n", arr1[2]);
			printf("arr3: %f\n", arr1[3]);
			printf("arr4: %f\n", arr1[4]);
			printf("arr5: %f\n", arr1[5]);
			printf("arr6: %f\n", arr1[6]);
			printf("arr7: %f\n", arr1[7]);
			printf("arr8: %f\n", arr1[8]);
			
			prediccion=predict(arr1);
			
			printf("Resultado: %f\n", prediccion);
			
			web_alert(prediccion); // Observar y pensar comportamiento frente a falsos positivos #########################################################

			
			prediccion_sum+=prediccion;
			
			
			if(prediccion_sum==5){ 
				
				llamar=1;
				prediccion_sum=0;
				
				}
				
			
/*			
arr0: 0.000000
arr1: 0.000000
arr2: 0.000000
arr3: 0.000000
arr4: 100.000000
arr5: 1.000000
arr6: 0.000000
arr7: 100.000000
arr8: 1.000000
	*/		
	
			/*
			double par[]={0, 0, 0, 0, 100, 1, 0, 100, 0};
	
			double resultado=predict(par);
			printf("Resultado: %f\n", resultado);
			
			web_alert(resultado);
			
			prediccion_sum+=resultado;
			
			if(prediccion_sum==5){
				
				llamar=1;
				prediccion_sum=0;
				
				}
			*/
			/*###########################################################################################*/
			/*
			arr1[0]=50;
			arr1[1]=30;
			arr1[2]=25;
			arr1[3]=76;
			arr1[4]=66;
			arr1[5]=24;
			arr1[6]=99;
			arr1[7]=83;
			arr1[8]=44;
			*/
			indicadorhp=0;
			indicadorts=0;
			indicador=0;
			
			por_srv_rerror=-1;
			por_rerror_rate=-1;
			por_srv_serror=-1;
			por_cant_dstsrc_srv=-1;
			cant_dst_host=-1;
			por_srv_diff_host_rate=-1;
			por_diffsrv_rate=-1;
			por_srv_rate=-1;
			por_serror_rate=-1;
			
			if(cant_copia==64000){
				paso_limite_copia=1;
				cant_copia=0;
				}
			
			
			}
		
	//memcpy(&arr_copia, &arr_fivetuple, sizeof(arr_fivetuple) );
	pthread_mutex_unlock(&lock);
	}
}

int cant_anomalies=0;
int net_state=0;


void web_alert(double prediccion){
	
mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *doc;

char *str_anomalies;
int anomalies=0;

int i=0;

double state_prob=0.0;

anomalies=check();

mongoc_init ();
client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "states");
	
	if(prediccion){
		cant_anomalies++;
		net_state=1;
	}
	else if(!anomalies){
		
		net_state=0;
		cant_anomalies=0;
		printf("Mando a 0 desde sniffer\n");	
		}


	   doc = bson_new ();
	   
	   if (!mongoc_collection_delete_one (
			 collection, doc, NULL, NULL, &error)) {
		  fprintf (stderr, "Delete failed: %s\n", error.message);
	   }

	   bson_destroy (doc);


			doc = bson_new ();
			bson_oid_init (&oid, NULL);
			
			/*################################################*/
			state_prob=state_dec;
			
			if(state_prob>1.0){
				state_prob=1.0;
				}
			
			if(state_prob<-1.0){
				state_prob=-1.0;
				}
			
			state_prob= 1.0-((state_prob+1.0)/2.0);
			
			/*################################################*/
			
				BSON_APPEND_INT32 (doc, "cant_anomalies",cant_anomalies);
				BSON_APPEND_BOOL(doc, "net_state",net_state);
				BSON_APPEND_DOUBLE (doc, "state_dec",state_prob);
		
			
			if (!mongoc_collection_insert_one (
				   collection, doc, NULL, NULL, &error)) {
				fprintf (stderr, "%s\n", error.message);
			}
			
			
bson_destroy (doc);

mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();

	}
	
int check(){	
		
mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *filter;
bson_t *opts;
const bson_t *doc;
mongoc_cursor_t *cursor;
char *str_anomalies;
int anomalies=0;

int i=0;

mongoc_init ();

client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "states");
	
	filter=bson_new ();
		
		opts=BCON_NEW("projection", "{","_id", BCON_BOOL(false),"cant_anomalies", BCON_BOOL(true),"}");
		
		
		cursor= mongoc_collection_find_with_opts(collection,filter, opts, NULL);
		
		while(mongoc_cursor_next(cursor, &doc)){
			
			str_anomalies = bson_as_json(doc, NULL);
			
			for(i=0; i<strlen(str_anomalies);i++){
				
				if(str_anomalies[i]>='0' && str_anomalies[i]<='9'){
					
					anomalies= anomalies*10 + str_anomalies[i]-'0';
					
					}
				}
			}
		
		if(mongoc_cursor_error (cursor, &error)){
			fprintf(stderr, "An error occurred: %s\n", error.message);
			
			}
			
			
mongoc_cursor_destroy(cursor);	
bson_destroy (filter);
bson_destroy (opts);
mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();

return anomalies;
}	
/*##########################################################################################################################################*/
/*SVM*/



int svm_get_svm_type(const struct svm_model *model)
{
	return model->param.svm_type;
}

int svm_get_nr_class(const struct svm_model *model)
{
	return model->nr_class;
}

double svm_get_svr_probability(const struct svm_model *model)
{
	if ((model->param.svm_type == EPSILON_SVR || model->param.svm_type == NU_SVR) &&
	    model->probA!=NULL)
		return model->probA[0];
	else
	{
		fprintf(stderr,"Model doesn't contain information for SVR probability inference\n");
		return 0;
	}
}


void svm_get_labels(const struct svm_model *model, int* label)
{
	if (model->label != NULL)
		for(int i=0;i<model->nr_class;i++)
			label[i] = model->label[i];
}

double svm_predict(const struct svm_model *model, const struct svm_node *x)
{
	
	int nr_class = model->nr_class;
	double *dec_values;
	if(model->param.svm_type == ONE_CLASS ||
	   model->param.svm_type == EPSILON_SVR ||
	   model->param.svm_type == NU_SVR){
		dec_values = (double*)malloc(1*sizeof(double));}
	else{
		dec_values = (double*)malloc(((double)nr_class*(((double)nr_class)-1.0)/2.0)*sizeof(double));
		}
		
	double pred_result = svm_predict_values(model, x, dec_values);

	free(dec_values);
	return pred_result;
}
	
double svm_predict_values(const struct svm_model *model, const struct svm_node *x, double* dec_values)
{
	
	int i;
	if(model->param.svm_type == ONE_CLASS ||
	   model->param.svm_type == EPSILON_SVR ||
	   model->param.svm_type == NU_SVR)
	{
		double *sv_coef = model->sv_coef[0];
		double sum = 0;
		for(i=0;i<model->l;i++)
			sum += sv_coef[i] * k_function(x,model->SV[i],model->param);
		sum -= model->rho[0];
		*dec_values = sum;
		
		if(model->param.svm_type == ONE_CLASS)
			return (sum>0)?1:-1;
		else
			return sum;
	}
	else
	{
		int nr_class = model->nr_class;
		int l = model->l;
	
		double *kvalue = (double*)malloc(l*sizeof(double));
		
		for(i=0;i<l;i++)
			kvalue[i] = k_function(x,model->SV[i],model->param);
			

		int *start = (int*)malloc(nr_class*sizeof(int));
		start[0] = 0;
		for(i=1;i<nr_class;i++)
			start[i] = start[i-1]+model->nSV[i-1];

		int *vote = (int*)malloc(nr_class*sizeof(int));
		for(i=0;i<nr_class;i++)
			vote[i] = 0;

		int p=0;
		for(i=0;i<nr_class;i++)
			for(int j=i+1;j<nr_class;j++)
			{
				double sum = 0;
				int si = start[i];
				int sj = start[j];
				int ci = model->nSV[i];
				int cj = model->nSV[j];

				int k;
				double *coef1 = model->sv_coef[j-1];
				double *coef2 = model->sv_coef[i];
				for(k=0;k<ci;k++)
					sum += coef1[si+k] * kvalue[si+k];
				for(k=0;k<cj;k++)
					sum += coef2[sj+k] * kvalue[sj+k];
				sum -= model->rho[p];
				dec_values[p] = sum;

				if(dec_values[p] > 0)
					++vote[i];
				else
					++vote[j];
				p++;
			}
		state_dec=dec_values[0];
		
		//printf("vaorr %lf\n", state_dec);
		int vote_max_idx = 0;
		for(i=1;i<nr_class;i++)
			if(vote[i] > vote[vote_max_idx])
				vote_max_idx = i;

		free(kvalue);
		free(start);
		free(vote);
		return model->label[vote_max_idx];
	}
}
int res=0;
static double k_function(const struct svm_node *x, const struct svm_node *y ,const struct svm_parameter param)
{	
	
	
	switch(param.kernel_type)
	{
		case LINEAR:
			return dot(x,y);
		case POLY:
			return powi(param.gamma*dot(x,y)+param.coef0,param.degree);
		case RBF:
		{
			double sum = 0;
			while(x->index != -1 && y->index !=-1)
			{
				if(x->index == y->index)
				{
					double d = x->value - y->value;
					sum += d*d;
					++x;
					++y;
					
				}
				else
				{
					if(x->index > y->index)
					{
						sum += y->value * y->value;
						++y;
					}
					else
					{
						sum += x->value * x->value;
						++x;
					}
				}
			}

			while(x->index != -1)
			{
				sum += x->value * x->value;
				++x;
			}

			while(y->index != -1)
			{
				sum += y->value * y->value;
				++y;
			}

			return exp(-param.gamma*sum);
		}
		case SIGMOID:
			return tanh(param.gamma*dot(x,y)+param.coef0);
		case PRECOMPUTED:  //x: test (validation), y: SV
			return x[(int)(y->value)].value;
		default:
			return 0;  // Unreachable
	}
}

static double sigmoid_predict(double decision_value, double A, double B)
{
	double fApB = decision_value*A+B;
	// 1-p used later; avoid catastrophic cancellation
	if (fApB >= 0)
		return exp(-fApB)/(1.0+exp(-fApB));
	else
		return 1.0/(1.0+exp(fApB)) ;
}

static const char *svm_type_table[] =
{
	"c_svc","nu_svc","one_class","epsilon_svr","nu_svr",NULL
};

static const char *kernel_type_table[]=
{
	"linear","polynomial","rbf","sigmoid","precomputed",NULL
};

struct svm_model *svm_load_model(const char *model_file_name)
{
	FILE *fp = fopen(model_file_name,"rb");
	if(fp==NULL) return NULL;

	char *old_locale = setlocale(LC_ALL, NULL);
	if (old_locale) {
		old_locale = strdup(old_locale);
	}
	setlocale(LC_ALL, "C");

	// read parameters

	struct svm_model *model = (struct svm_model*)malloc(1*sizeof(struct svm_model));
	model->rho = NULL;
	model->probA = NULL;
	model->probB = NULL;
	model->sv_indices = NULL;
	model->label = NULL;
	model->nSV = NULL;

	// read header
	if (!read_model_header(fp, model))
	{
		fprintf(stderr, "ERROR: fscanf failed to read model\n");
		setlocale(LC_ALL, old_locale);
		free(old_locale);
		free(model->rho);
		free(model->label);
		free(model->nSV);
		free(model);
		return NULL;
	}

	// read sv_coef and SV

	int elements = 0;
	long pos = ftell(fp);

	max_line_len = 1024;
	line = (char*)malloc(max_line_len*sizeof(char));
	char *p,*endptr,*idx,*val;

	while(readline(fp)!=NULL)
	{
		p = strtok(line,":");
		while(1)
		{
			p = strtok(NULL,":");
			if(p == NULL)
				break;
			++elements;
		}
	}
	elements += model->l;

	fseek(fp,pos,SEEK_SET);

	int m = model->nr_class - 1;
	int l = model->l;
	
	model->sv_coef = (double**) malloc(m*sizeof(double*));
	int i;
	for(i=0;i<m;i++)
		model->sv_coef[i] = (double*)malloc(l*sizeof(double));
	model->SV = (struct svm_node **) malloc(l*sizeof(struct svm_node*));
	struct svm_node *x_space = NULL;
	if(l>0) x_space = (struct svm_node*)malloc(elements*sizeof(struct svm_node));

	int j=0;
	for(i=0;i<l;i++)
	{
		readline(fp);
		model->SV[i] = &x_space[j];

		p = strtok(line, " \t");
		model->sv_coef[0][i] = strtod(p,&endptr);
		for(int k=1;k<m;k++)
		{
			p = strtok(NULL, " \t");
			model->sv_coef[k][i] = strtod(p,&endptr);
		}

		while(1)
		{
			idx = strtok(NULL, ":");
			val = strtok(NULL, " \t");

			if(val == NULL)
				break;
			x_space[j].index = (int) strtol(idx,&endptr,10);
			x_space[j].value = strtod(val,&endptr);

			++j;
		}
		x_space[j++].index = -1;
	}
	free(line);

	setlocale(LC_ALL, old_locale);
	free(old_locale);

	if (ferror(fp) != 0 || fclose(fp) != 0)
		return NULL;

	model->free_sv = 1;	// XXX
	
	return model;
}

#define FSCANF(_stream, _format, _var) do{ if (fscanf(_stream, _format, _var) != 1) return false; }while(0)
bool read_model_header(FILE *fp, struct svm_model* model)
{
	struct svm_parameter *param = &model->param;

	// parameters for training only won't be assigned, but arrays are assigned as NULL for safety
	param->nr_weight = 0;
	param->weight_label = NULL;
	param->weight = NULL;

	char cmd[81];
	while(1)
	{
		FSCANF(fp,"%80s",cmd);

		if(strcmp(cmd,"svm_type")==0)
		{
			FSCANF(fp,"%80s",cmd);
			int i;
			for(i=0;svm_type_table[i];i++)
			{
				if(strcmp(svm_type_table[i],cmd)==0)
				{
					param->svm_type=i;
					break;
				}
			}
			if(svm_type_table[i] == NULL)
			{
				fprintf(stderr,"unknown svm type.\n");
				return false;
			}
		}
		else if(strcmp(cmd,"kernel_type")==0)
		{
			FSCANF(fp,"%80s",cmd);
			int i;
			for(i=0;kernel_type_table[i];i++)
			{
				if(strcmp(kernel_type_table[i],cmd)==0)
				{
					param->kernel_type=i;
					
					

					break;
				}
			}
			if(kernel_type_table[i] == NULL)
			{
				fprintf(stderr,"unknown kernel function.\n");
				return false;
			}
		}
		else if(strcmp(cmd,"degree")==0)
			FSCANF(fp,"%d",&param->degree);
		else if(strcmp(cmd,"gamma")==0)
			FSCANF(fp,"%lf",&param->gamma);
		else if(strcmp(cmd,"coef0")==0)
			FSCANF(fp,"%lf",&param->coef0);
		else if(strcmp(cmd,"nr_class")==0)
			FSCANF(fp,"%d",&model->nr_class);
		else if(strcmp(cmd,"total_sv")==0)
			FSCANF(fp,"%d",&model->l);
		else if(strcmp(cmd,"rho")==0)
		{
			int n = model->nr_class * (model->nr_class-1)/2;
			model->rho = (double*) malloc(n*sizeof(double));
			for(int i=0;i<n;i++)
				FSCANF(fp,"%lf",&model->rho[i]);
		}
		else if(strcmp(cmd,"label")==0)
		{
			int n = model->nr_class;
			model->label = (int*)malloc(n*sizeof(int));
			for(int i=0;i<n;i++)
				FSCANF(fp,"%d",&model->label[i]);
		}
		else if(strcmp(cmd,"probA")==0)
		{
			int n = model->nr_class * (model->nr_class-1)/2;
			model->probA = (double*) malloc(n*sizeof(double));
			for(int i=0;i<n;i++)
				FSCANF(fp,"%lf",&model->probA[i]);
		}
		else if(strcmp(cmd,"probB")==0)
		{
			int n = model->nr_class * (model->nr_class-1)/2;
			model->probB = (double*)malloc(n*sizeof(double));
			for(int i=0;i<n;i++)
				FSCANF(fp,"%lf",&model->probB[i]);
		}
		else if(strcmp(cmd,"nr_sv")==0)
		{
			int n = model->nr_class;
			model->nSV = (int*)malloc(n*sizeof(int));
			for(int i=0;i<n;i++)
				FSCANF(fp,"%d",&model->nSV[i]);
		}
		else if(strcmp(cmd,"SV")==0)
		{
			while(1)
			{
				int c = getc(fp);
				if(c==EOF || c=='\n') break;
			}
			break;
		}
		else
		{
			fprintf(stderr,"unknown text in model file: [%s]\n",cmd);
			return false;
		}
	}

	return true;

}

int svm_check_probability_model(const struct svm_model *model)
{
	return ((model->param.svm_type == C_SVC || model->param.svm_type == NU_SVC) &&
		model->probA!=NULL && model->probB!=NULL) ||
		((model->param.svm_type == EPSILON_SVR || model->param.svm_type == NU_SVR) &&
		 model->probA!=NULL);
}

void svm_free_and_destroy_model(struct svm_model** model_ptr_ptr)
{
	if(model_ptr_ptr != NULL && *model_ptr_ptr != NULL)
	{
		svm_free_model_content(*model_ptr_ptr);
		free(*model_ptr_ptr);
		*model_ptr_ptr = NULL;
	}
}

void svm_free_model_content(struct svm_model* model_ptr)
{
	if(model_ptr->free_sv && model_ptr->l > 0 && model_ptr->SV != NULL)
		free((void *)(model_ptr->SV[0]));
	if(model_ptr->sv_coef)
	{
		for(int i=0;i<model_ptr->nr_class-1;i++)
			free(model_ptr->sv_coef[i]);
	}

	free(model_ptr->SV);
	model_ptr->SV = NULL;

	free(model_ptr->sv_coef);
	model_ptr->sv_coef = NULL;

	free(model_ptr->rho);
	model_ptr->rho = NULL;

	free(model_ptr->label);
	model_ptr->label= NULL;

	free(model_ptr->probA);
	model_ptr->probA = NULL;

	free(model_ptr->probB);
	model_ptr->probB= NULL;

	free(model_ptr->sv_indices);
	model_ptr->sv_indices = NULL;

	free(model_ptr->nSV);
	model_ptr->nSV = NULL;
}

double dot(const struct svm_node *px, const struct svm_node *py)
{
	double sum = 0;
	while(px->index != -1 && py->index != -1)
	{
		if(px->index == py->index)
		{
			sum += px->value * py->value;
			++px;
			++py;
		}
		else
		{
			if(px->index > py->index)
				++py;
			else
				++px;
		}
	}
	return sum;
}

static inline double powi(double base, int times)
{
	double tmp = base, ret = 1.0;

	for(int t=times; t>0; t/=2)
	{
		if(t%2==1) ret*=tmp;
		tmp = tmp * tmp;
	}
	return ret;
}


static char* readline(FILE *input)
{
	int len;

	if(fgets(line,max_line_len,input) == NULL)
		return NULL;

	while(strrchr(line,'\n') == NULL)
	{
		max_line_len *= 2;
		line = (char *) realloc(line,max_line_len);
		len = (int) strlen(line);
		if(fgets(line+len,max_line_len-len,input) == NULL)
			break;
	}
	return line;
}

void exit_input_error(int line_num)
{
	fprintf(stderr,"Wrong input format at line %d\n", line_num);
	exit(1);
}

double predict(double par[])
{
	int lim_par=9;
	int svm_type=svm_get_svm_type(model);
	int nr_class=svm_get_nr_class(model);
	double predict_label;
	char *idx, *val;
	int i = 0;
		
		while(i<lim_par)
		{
			x = (struct svm_node *) realloc(x,max_nr_attr*sizeof(struct svm_node));
			
			x[i].index = i+1;
			x[i].value = par[i];
			++i;
			
		}
		x[i].index = -1;
		
		predict_label = svm_predict(model,x);
			
		return predict_label;
}

void exit_with_help()
{
	printf(
	"Usage: svm-predict [options] test_file model_file output_file\n"
	"options:\n"
	"-b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); for one-class SVM only 0 is supported\n"
	"-q : quiet mode (no outputs)\n"
	);
	exit(1);
}

/*###################################################################################################################*/

/*###################################################################################################################*/
/*GPRS*/

 
void *get_telephone(){

int called=0;

while(1){
	pthread_mutex_lock(&lock);
	
	if(llamar){

mongoc_client_t *client;
mongoc_collection_t *collection;
bson_error_t error;
bson_oid_t oid;
bson_t *filter;
bson_t *opts;
const bson_t *doc;
mongoc_cursor_t *cursor;
char *str_tel;
char *temptel;
long long tel;
char tel_char[10];
char envio[11];
int i=0;


//char *ss= "hola 98";
//char *format="{ \"telephone\" : \"%d\" }";

mongoc_init ();
client = mongoc_client_new ("mongodb://localhost:27017/");
collection = mongoc_client_get_collection (client, "visual", "telephones");

		//filter=BCON_NEW("owner", BCON_UTF8 ("Pablo"));
		filter=bson_new ();
		//opts=BCON_NEW("projection", "{","_id", BCON_BOOL(false),"owner", BCON_BOOL(false),"telephone", BCON_BOOL(true),"}");
		opts=BCON_NEW("projection", "{","_id", BCON_BOOL(false),"telephone", BCON_BOOL(true),"}");
		
		
		cursor= mongoc_collection_find_with_opts(collection,filter, opts, NULL);
		
		while(mongoc_cursor_next(cursor, &doc)){
			
			tel=0;
			memset(envio, 0, strlen(envio));
			memset(tel_char, 0, strlen(tel_char));
			str_tel = bson_as_canonical_extended_json(doc, NULL);
			//sscanf(str_tel, "%s %d %s",format1, &tel, format2);
			//sscanf(str_tel, "{ \"telephone\" : \"%d\" }",&tel);
			//sscanf(str_tel, format, &tel);
			
			//printf("%d\n", tel);
			
			for(i=0; i<strlen(str_tel);i++){
				
				if(str_tel[i]>='0' && str_tel[i]<='9'){
					
					tel= tel*10 + str_tel[i]-'0';
					//printf("%llu\n", tel);
					
					
					}
				
				}
				
			//printf("%llu\n", tel);
			sprintf(tel_char, "%lld", tel);
			envio[0]='+';
			strcat(envio, tel_char);
			
			call_t (envio);
			//printf("llamando...\n");
			//printf("%s\n", envio);
			//printf("%d\n", tel);
			//printf("%s\n", str_tel);

			
			}
		
		if(mongoc_cursor_error (cursor, &error)){
			fprintf(stderr, "An error occurred: %s\n", error.message);
			
			}

mongoc_cursor_destroy(cursor);	
bson_destroy (filter);
bson_destroy (opts);
mongoc_collection_destroy (collection);
mongoc_client_destroy (client);
mongoc_cleanup ();

llamar=0;
called=1;
	}
	pthread_mutex_unlock(&lock);
	
	if(called){
	sleep(60);
	called=0;
	}
} 
}
 
int call_t (char* tel)
{
  int serial_port,i=0 ;
  char datmes1[]="AT+CMGS=";
  char mensaje[]="NetLynx: Anomalia detectada\032";
  if ((serial_port = serialOpen ("/dev/ttyS0", 115200)) < 0)	/* open serial port */
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

  if (wiringPiSetup () == -1)					/* initializes wiringPi setup */
  {
    fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno)) ;
    return 1 ;
  }

		/*for(i=0;i<17;i++)
		{
		printf ("%c", dat[i]) ;
		fflush (stdout) ;
		serialPutchar(serial_port, dat[i]);		
        }
        sleep(10);
        serialPutchar(serial_port,'A');
        serialPutchar(serial_port,'T');	
        serialPutchar(serial_port,'H');	
        serialPutchar(serial_port,'\r');	
        sleep(10);*/
    ////////////////////////////////////////////////mensaje    
       for(i=0;i<8;i++)
		{
		printf ("%c", datmes1[i]) ;
		fflush (stdout) ;
		serialPutchar(serial_port, datmes1[i]);		
        }
        printf ("%c", '"' );
        serialPutchar(serial_port,'"');	
        for(i=0;i<11;i++)
		{
		printf ("%c", tel[i]) ;
		fflush (stdout) ;
		serialPutchar(serial_port, tel[i]);		
        }
        printf ("%c", '"' );
        serialPutchar(serial_port,'"');	
		serialPutchar(serial_port,'\r');
		
		sleep(2);
		
		
		for(i=0;i<strlen(mensaje)+3;i++)
		{
		printf ("%c", mensaje[i]) ;
		fflush (stdout) ;
		serialPutchar(serial_port, mensaje[i]);		
        }	
		//serialPutchar(serial_port, '\032');		
        sleep(4);
}


/*###################################################################################################################*/
